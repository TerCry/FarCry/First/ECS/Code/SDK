# SDK

TODO

# Dependencies

* [bake](https://github.com/SanderMertens/bake) - build system

# Setup

`bake -r build`

## Environment Variables

- `BAKE_ARCHITECTURE=x86`
- `BAKE_CONFIG=release`
- `BAKE_HOME=%HOMEPATH%/bake`
- `Path=%Path%;%BAKE_HOME%/lib;%BAKE_HOME%/%BAKE_ARCHITECTURE%-Windows/%BAKE_CONFIG%/lib`

Restart is required after environment variables setup.

## VC++ Directories

### Include files

 - `%BAKE_HOME%/include/farcry-sdk/legacy/STLPORT`
 - `%BAKE_HOME%/include/farcry-sdk/legacy/STLPORT/stlport`
 - `%BAKE_HOME%/include/farcry-sdk/legacy`
 - **...**
 - `%BAKE_HOME%/include`

### Library files

 - `%BAKE_HOME%/lib`
 - `%BAKE_HOME%/%BAKE_ARCHITECTURE%-Windows/%BAKE_CONFIG%/lib`

## Preprocessor definitions

 - `__BAKE_LEGACY__`