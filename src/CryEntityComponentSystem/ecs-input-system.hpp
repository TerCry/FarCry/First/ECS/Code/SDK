#pragma once

#include <IInput.h>

struct EcsKeyState;

void ecs_cry_editor_input_key_setup();
void ecs_cry_gameplay_input_key_setup();

bool ecs_cry_is_input_key_down(EcsKeyState *key);
bool ecs_cry_is_input_key_up(EcsKeyState *key);

void ecs_cry_any_input_key_down(EcsKeyState *key);
void ecs_cry_any_input_key_up(EcsKeyState *key);
void ecs_cry_any_input_key_reset(EcsKeyState *state);

const KeyCodes& ecs_cry_gameplay_input_key_code(int index);
const uint8&    ecs_cry_editor_input_key_code(int index);