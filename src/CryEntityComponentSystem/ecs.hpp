#pragma once

#include <farcry_sdk.h>
#include <cstring>

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS C++ private implementation
//////////////////////////////////////////////////////////////////////////////////////////////

namespace _ecs 
{
    template<typename Family>
    struct family {
        static uint32_t identifier() {
            static uint32_t value = 0;
            return value++;
        }

        template<typename>
        static uint32_t type() {
            static const uint32_t value = identifier();
            return value;
        }
    };

    template<typename Component>
    class component_wrapper 
    {
    public:
        component_wrapper() { }
        component_wrapper(const component_wrapper& rhs) : value(rhs.value) { }
        template<typename T>
        component_wrapper(const T& rhs) : value(rhs) { }

        component_wrapper& operator = (const component_wrapper& rhs) {
            value = rhs.value; 
            return *this;
        }
        component_wrapper& operator = (const Component& rhs) {
            value = rhs;
            return *this;
        }

        operator Component&() {
            return value;
        }
        operator const Component&() const {
            return value;
        }
        
        operator Component*() {
            return &value;
        }
        operator const Component*() const {
            return &value;
        }

        Component* operator -> () {
            return &value;
        }
        const Component* operator -> () const {
            return &value;
        }
    private:
        Component value;
    };


    struct handle {
        ecs_entity_t entity;
        ecs_type_t	 type;
    };

    struct ptr_handle {
        const handle* component;
        size_t size;
        void *ptr;
    };


    class typesafe_handles {
    public:
        typesafe_handles() {
            table = ecs_vector_new(&params, 0);
        }

        ~typesafe_handles() {
            ecs_vector_free(table);
        }
        
        template<typename T>
        static uint32_t get_index() {
            return family<EcsComponent>::type<T>();
        }


        template<typename T>
        handle* get_or_create() {
            uint32_t index = get_index<T>();
            uint32_t count = ecs_vector_count(table);
            
            if (count <= index) {
                ecs_vector_set_count(&table, &params, index + 1);
            }
            
            return (handle*)ecs_vector_get(table, &params, index);
        }
        template<typename T>
        const handle* get() const {
            uint32_t index = get_index<T>();
            uint32_t count = ecs_vector_count(table);

            if (count <= index) {
                return NULL;
            }
            
            return (const handle*)ecs_vector_get(table, &params, index);
        }
        template<typename T>
        const handle* set(ecs_entity_t entity) {
            handle* h = get_or_create<T>();

            h->entity = entity;
            h->type = ecs_type_from_entity(world, entity);

            return h;
        }
    private:
        ecs_vector_t*		table;
        static ecs_vector_params_t params;
    public:
        ecs_world_t*	world;
    };

    

    class type_builder
    {
    public:
        type_builder(typesafe_handles* handles);
        ~type_builder();

        type_builder& add(ecs_entity_t entity);
        type_builder& add(const ptr_handle* begin, const ptr_handle* end);

        type_builder& childof(ecs_entity_t entity);
        type_builder& instanceof(ecs_entity_t entity);

        type_builder& prefab();
        type_builder& hidden();
        type_builder& disabled();

        ecs_type_t   build();
        ecs_entity_t build(EcsId id);

        template<typename T>
        type_builder& typeof() {
            return add(handles->get<T>()->entity);
        } 
        template<typename T>
        type_builder& typeof(const T&) {
            return typeof<T>();
        } 
        template<typename T>
        type_builder& typeof(const T*) {
            return typeof<T>();
        }
    private:
        ecs_vector_t* array;
        static ecs_vector_params_t params;
    public:
        typesafe_handles* handles;
    };
    
    
    class ptr_builder 
    {	
    public:
        ptr_builder(typesafe_handles* handles);
        ~ptr_builder();

        ptr_handle* begin();
        ptr_handle* end();

        const ptr_handle* begin() const;
        const ptr_handle* end() const;

        ptr_builder& add(const ptr_handle& handle);
        ptr_builder& set(ecs_entity_t entity);
        ptr_builder& get(ecs_entity_t entity);
        ptr_builder& clear();

        template<typename T>
        ptr_builder& add(T* ptr) {
            ptr_handle handle = {handles->get<T>(), sizeof(T), ptr};

            return add(handle);
        }
        template<typename T>
        ptr_builder& add(const T* ptr) {
            return add<T>(const_cast<T*>(ptr));
        }
    private:
        ecs_vector_t* array;
        static ecs_vector_params_t params;
    public:
        typesafe_handles* handles;
    };

    
    class entity_builder
    {
    public:
        entity_builder(typesafe_handles* handles);
        ~entity_builder();

        entity_builder& add(ecs_entity_t entity);

        entity_builder& childof(ecs_entity_t entity);
        entity_builder& instanceof(ecs_entity_t entity);

        entity_builder& prefab();
        entity_builder& hidden();
        entity_builder& disabled();

        ecs_entity_t build(EcsId id);

        template<typename Component>
        entity_builder& use(Component* component) {
            ptr.add<Component>(component);

            return *this;
        }

        template<typename Component>
        entity_builder& add() {
            type.typeof<Component>();
            
            return *this;
        } 
    private:
        ptr_builder     ptr;
        type_builder    type;
    };


    class system_builder
    {
    public:
        system_builder(typesafe_handles* handles);

        system_builder& optional();
        system_builder& and();
        system_builder& not();
        system_builder& or();

        system_builder& typeof(ecs_entity_t entity);

        system_builder& in(ecs_entity_t entity);
        system_builder& out(ecs_entity_t entity);
        system_builder& self(ecs_entity_t entity);
        system_builder& owned(ecs_entity_t entity);
        system_builder& shared(ecs_entity_t entity);
        system_builder& nothing(ecs_entity_t entity);
        system_builder& container(ecs_entity_t entity);
        system_builder& cascade(ecs_entity_t entity);
        system_builder& system(ecs_entity_t entity);
        system_builder& singleton(ecs_entity_t entity);
        system_builder& entity(ecs_entity_t entity, ecs_entity_t component);

        ecs_entity_t build(EcsId id, EcsSystemKind kind, ecs_system_action_t action);

        template<typename T>
        system_builder& typeof() {
            return typeof(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& in() {
            return in(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& out() {
            return out(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& self() {
            return self(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& owned() {
            return owned(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& shared() {
            return shared(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& nothing() {
            return nothing(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& container() {
            return container(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& cascade() {
            return cascade(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& system() {
            return system(handles->get<T>()->entity);
        }

        template<typename T>
        system_builder& singleton() {
            return singleton(handles->get<T>()->entity);
        }
        template<typename T>
        system_builder& entity(ecs_entity_t entity) {
            return ref(entity, handles->get<T>()->entity);
        }
    private:
        system_builder& str(const char* str);
        system_builder& src(const char* prefix, ecs_entity_t entity);
        system_builder& ref(ecs_entity_t entity, ecs_entity_t component);
        char* build();
    private:
        typesafe_handles* handles;
        ecs_strbuf_t signature;
    };
}

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS type-safe redefinition helpers 
//////////////////////////////////////////////////////////////////////////////////////////////
#ifndef ecs_component 
#define ecs_component _ecs_component
#endif

#define ecs_define_tag(_,id) \
    namespace ecs_component { struct id; } typedef ecs_component::id id;

#define ecs_define_component(_,id) \
    namespace ecs_component { \
        struct id : public _ecs::component_wrapper<::id> { \
            using _ecs::component_wrapper<::id>::operator =;\
            id() { }\
            template<typename T>\
            id(const T& rhs) : _ecs::component_wrapper<::id>(rhs) { }\
        };\
    }

#define ecs_define_nothing(_,id)

#define ecs_define_module(id) \
    id##ImportHandles(void)

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS type-safe importing helpers 
//////////////////////////////////////////////////////////////////////////////////////////////

#define ecs_import_tag(_, id) \
    handles.set<ecs_component::id>(h->id);

#define ecs_import_component(_, id) \
    handles.set<ecs_component::id>(h->ecs_entity(id));

#define ecs_import_type(_, id) \
    handles.set<id>(h->ecs_entity(id));

#define ecs_import_module(id)\
    template<> void ecs::import<id>(int flags) {\
        handles.set<id>(_ecs_import(world, id##Import, #id, flags, 0, 0));\
        id* h = get<id>(ECS_SINGLETON);\
        id##ImportHandles(void)\
    }\


//////////////////////////////////////////////////////////////////////////////////////////////
// ECS C++ header implementation
//////////////////////////////////////////////////////////////////////////////////////////////

class ecs {
private:
    _ecs::typesafe_handles	handles;
    void*					context;
    bool					owned;
    ecs_world_t*			world;
private:
    void ctor(ecs_world_t* world, bool owned);
public:

    ecs(int argc, char *argv[]);
    ecs(ecs_world_t* world = NULL, bool owned = false);

    ~ecs();

    static ecs* get(ecs_world_t* world);

    ecs_world_t* get_world();

    _ecs::type_builder      new_type_builder();
    _ecs::ptr_builder       new_ptr_builder();
    _ecs::entity_builder    new_entity_builder();
    _ecs::system_builder    new_system_builder();

    ecs_entity_t new_entity(EcsId id, ecs_type_t type = 0);
    
    template<typename Context>
    void set(Context* context) {
        this->context = static_cast<void*>(context);
    }

    template<typename Context>
    Context* get() {
        return static_cast<Context*>(context);
    }
    
    template<typename Module>
    void import(int flags);

    template<typename Tag>
    const _ecs::handle* new_tag(EcsId id) {
        return handles.set<Tag>(ecs_new_component(world, id, 0));
    }

    template<typename Component>
    const _ecs::handle* new_component(EcsId id) {
        return handles.set<Component>(ecs_new_component(world, id, sizeof(Component)));
    }

    template<typename Component>
    const _ecs::handle* get_handle() {
        return handles.get<Component>();
    }

    template<typename Component>
    bool has(ecs_entity_t entity) {
        const _ecs::handle* h = handles.get<Component>();

        return _ecs_has(world, entity, h->type);
    }

    template<typename Component>
    void add(ecs_entity_t entity) {
        const _ecs::handle* h = handles.get<Component>();

        _ecs_add(world, entity, h->type);
    }

    template<typename Component>
    void set(ecs_entity_t entity, const Component& copy) {
        const _ecs::handle* h = handles.get<Component>();
        const void* ptr = &copy;

        _ecs_set_ptr(world, entity, h->entity, sizeof(Component), const_cast<void*>(ptr));
    }

    template<typename Component>
    Component* get(ecs_entity_t entity) {
        const _ecs::handle* h = handles.get<Component>();
        void* ptr = _ecs_get_ptr(world, entity, h->type);

        return static_cast<Component*>(ptr);
    }

    template<typename Component>
    void remove(ecs_entity_t entity) {
        const _ecs::handle* h = handles.get<Component>();

        _ecs_remove(world, entity, h->type);
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS core handles
//////////////////////////////////////////////////////////////////////////////////////////////
#define FlecsImportHandles(handles)\
    ECS_IMPORT_COMPONENT(handles, EcsId);\
    ECS_IMPORT_COMPONENT(handles, EcsComponent);\
    ECS_IMPORT_COMPONENT(handles, EcsTypeComponent);\
    ECS_IMPORT_ENTITY(handles, EcsHidden);\
    ECS_IMPORT_ENTITY(handles, EcsDisabled);\
    ECS_IMPORT_ENTITY(handles, EcsOnDemand);

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS core prepare
//////////////////////////////////////////////////////////////////////////////////////////////
#undef ECS_IMPORT
#undef ECS_IMPORT_COMPONENT
#undef ECS_IMPORT_ENTITY
//////////////////////////////////////////////////////////////////////////////////////////////
// ECS type-safe core redefinition 
//////////////////////////////////////////////////////////////////////////////////////////////
#define ECS_IMPORT_COMPONENT ecs_define_component
#define ECS_IMPORT_ENTITY ecs_define_tag

FlecsImportHandles(void)

#undef ECS_IMPORT_COMPONENT
#undef ECS_IMPORT_ENTITY

template<> void ecs::import<ecs_world_t>(int flags);
//////////////////////////////////////////////////////////////////////////////////////////////
// ECS legacy wrapper API reimplementation
//////////////////////////////////////////////////////////////////////////////////////////////
#define ECS_COLUMN_TYPE(rows, id, column)\
    ECS_TYPE_VAR(id) = ecs_column_type(rows, column);\
    (void)ecs_type(id)

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS new api implementation
//////////////////////////////////////////////////////////////////////////////////////////////

typedef uint32_t ecs_type_index_t;

bool ecs_type_next_index(ecs_type_t type, ecs_entity_t mask, ecs_type_index_t* index);