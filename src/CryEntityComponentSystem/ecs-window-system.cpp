#include "ecs-window-system.hpp"

#include <ISystem.h>
#include <IRenderer.h>

void ecs_cry_do_window_init(ecs_rows_t *rows) {
    ecs* self = ecs::get(rows->world);
    EcsCanvas2D *canvas = ecs_column(rows, EcsCanvas2D, 1);
    IRenderer *renderer = GetISystem()->GetIRenderer();

    EcsVec2 translate;
    
    
    int i;
    for (i = 0; i < rows->count; i ++) {
        const char *title = canvas->title;
        if (!title) {
            title = "FarCry window";
        }

       
        canvas->viewport = canvas->window;
        canvas->window_actual = canvas->window;
        
        ecs_cry_window wnd;
        
        ecs_vec2_set(
            &wnd.scale
            ,800.0f / (float)canvas->window_actual.width
            ,600.0f / (float)canvas->window_actual.height
        );
        ecs_vec2_set(
            &translate
            ,wnd.scale.x * ((float)canvas->window_actual.width / 2.0f)
            ,wnd.scale.y * ((float)canvas->window_actual.height / 2.0f)
        );
        
        ecs_mat3x3_set_identity(&wnd.screen);

        ecs_mat3x3_add_translation(
             &wnd.screen
            ,&translate
        );
        ecs_mat3x3_add_scale(
             &wnd.screen
            ,&wnd.scale
        );

        wnd.projection = wnd.screen;

        self->set(rows->entities[i], wnd);
        self->add<ecs_component::EcsInput>(rows->entities[i]);
    }
}

void ecs_cry_do_window_deinit(ecs_rows_t *rows) {
    ecs_cry_window *window = ecs_column(rows, ecs_cry_window, 1);
    int i;
    for (i = 0; i < rows->count; i ++) {

    }
}

void ecs_cry_window_integrate(ecs* self, int flags)
{
    self->new_component<ecs_cry_window>("ecs_cry_window");

    self->new_system_builder()
        .self<ecs_component::EcsCanvas2D>()
        .nothing<ecs_cry_window>()
        .nothing<ecs_component::EcsInput>()
        .system<ecs_component::EcsHidden>()
        .build("ecs_cry_do_window_init", EcsOnSet, &ecs_cry_do_window_init)
    ;
    
    self->new_system_builder()
        .self<ecs_cry_window>()
        .system<ecs_component::EcsHidden>()
        .build("ecs_cry_do_window_deinit", EcsOnRemove, &ecs_cry_do_window_deinit)
    ;
}