#include "ecs-input-system.hpp"

#include "ecs-modules.hpp"

static KeyCodes ecs_cry_gameplay_input_key_codes[128];
static uint8	ecs_cry_editor_input_key_codes[128];

void ecs_cry_editor_input_key_setup()
{
    ecs_cry_editor_input_key_codes[ECS_KEY_UNKNOWN]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_RETURN]		 = VK_RETURN;
    ecs_cry_editor_input_key_codes[ECS_KEY_ESCAPE]		 = VK_ESCAPE;
    ecs_cry_editor_input_key_codes[ECS_KEY_BACKSPACE]	 = VK_CLEAR;
    ecs_cry_editor_input_key_codes[ECS_KEY_TAB]			 = VK_TAB;
    ecs_cry_editor_input_key_codes[ECS_KEY_SPACE]		 = VK_SPACE;
    ecs_cry_editor_input_key_codes[ECS_KEY_EXCLAIM]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_QUOTEDBL]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_HASH]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_PERCENT]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_DOLLAR]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_AMPERSAND]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_QUOTE]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_LEFTPAREN]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_RIGHTPAREN]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_ASTERISK]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_PLUS]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_COMMA]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_MINUS]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_PERIOD]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_SLASH]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_0]			 = '0';
    ecs_cry_editor_input_key_codes[ECS_KEY_1]			 = '1';
    ecs_cry_editor_input_key_codes[ECS_KEY_2]			 = '2';
    ecs_cry_editor_input_key_codes[ECS_KEY_3]			 = '3';
    ecs_cry_editor_input_key_codes[ECS_KEY_4]			 = '4';
    ecs_cry_editor_input_key_codes[ECS_KEY_5]			 = '5';
    ecs_cry_editor_input_key_codes[ECS_KEY_6]			 = '6';
    ecs_cry_editor_input_key_codes[ECS_KEY_7]			 = '7';
    ecs_cry_editor_input_key_codes[ECS_KEY_8]			 = '8';
    ecs_cry_editor_input_key_codes[ECS_KEY_9]			 = '9';
    ecs_cry_editor_input_key_codes[ECS_KEY_COLON]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_SEMICOLON]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_LESS]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_EQUALS]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_GREATER]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_QUESTION]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_AT]			 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_LEFTBRACKET]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_BACKSLASH]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_RIGHTBRACKET] = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_CARET]		 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_UNDERSCORE]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_BACKQUOTE]	 = 0xFF;
    ecs_cry_editor_input_key_codes[ECS_KEY_A]			 = 'A';
    ecs_cry_editor_input_key_codes[ECS_KEY_B]			 = 'B';
    ecs_cry_editor_input_key_codes[ECS_KEY_C]			 = 'C';
    ecs_cry_editor_input_key_codes[ECS_KEY_D]			 = 'D';
    ecs_cry_editor_input_key_codes[ECS_KEY_E]			 = 'E';
    ecs_cry_editor_input_key_codes[ECS_KEY_F]			 = 'F';
    ecs_cry_editor_input_key_codes[ECS_KEY_G]			 = 'G';
    ecs_cry_editor_input_key_codes[ECS_KEY_H]			 = 'H';
    ecs_cry_editor_input_key_codes[ECS_KEY_I]			 = 'I';
    ecs_cry_editor_input_key_codes[ECS_KEY_J]			 = 'J';
    ecs_cry_editor_input_key_codes[ECS_KEY_K]			 = 'K';
    ecs_cry_editor_input_key_codes[ECS_KEY_L]			 = 'L';
    ecs_cry_editor_input_key_codes[ECS_KEY_M]			 = 'M';
    ecs_cry_editor_input_key_codes[ECS_KEY_N]			 = 'N';
    ecs_cry_editor_input_key_codes[ECS_KEY_O]			 = 'O';
    ecs_cry_editor_input_key_codes[ECS_KEY_P]			 = 'P';
    ecs_cry_editor_input_key_codes[ECS_KEY_Q]			 = 'Q';
    ecs_cry_editor_input_key_codes[ECS_KEY_R]			 = 'R';
    ecs_cry_editor_input_key_codes[ECS_KEY_S]			 = 'S';
    ecs_cry_editor_input_key_codes[ECS_KEY_T]			 = 'T';
    ecs_cry_editor_input_key_codes[ECS_KEY_U]			 = 'U';
    ecs_cry_editor_input_key_codes[ECS_KEY_V]			 = 'V';
    ecs_cry_editor_input_key_codes[ECS_KEY_W]			 = 'W';
    ecs_cry_editor_input_key_codes[ECS_KEY_X]			 = 'X';
    ecs_cry_editor_input_key_codes[ECS_KEY_Y]			 = 'Y';
    ecs_cry_editor_input_key_codes[ECS_KEY_Z]			 = 'Z';
    ecs_cry_editor_input_key_codes[ECS_KEY_DELETE]		 = VK_DELETE;
    ecs_cry_editor_input_key_codes[ECS_KEY_RIGHT]		 = VK_RIGHT;
    ecs_cry_editor_input_key_codes[ECS_KEY_LEFT]		 = VK_LEFT;
    ecs_cry_editor_input_key_codes[ECS_KEY_DOWN]		 = VK_DOWN;
    ecs_cry_editor_input_key_codes[ECS_KEY_UP]			 = VK_UP;
    ecs_cry_editor_input_key_codes[ECS_KEY_CTRL]		 = VK_LCONTROL;
    ecs_cry_editor_input_key_codes[ECS_KEY_SHIFT]		 = VK_LSHIFT;
    ecs_cry_editor_input_key_codes[ECS_KEY_ALT]			 = VK_LMENU;
}
void ecs_cry_gameplay_input_key_setup()
{
    ecs_cry_gameplay_input_key_codes[ECS_KEY_UNKNOWN]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_RETURN]		 = XKEY_RETURN;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_ESCAPE]		 = XKEY_ESCAPE;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_BACKSPACE]		 = XKEY_BACKSPACE;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_TAB]			 = XKEY_TAB;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_SPACE]			 = XKEY_SPACE;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_EXCLAIM]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_QUOTEDBL]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_HASH]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_PERCENT]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_DOLLAR]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_AMPERSAND]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_QUOTE]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_LEFTPAREN]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_RIGHTPAREN]	 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_ASTERISK]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_PLUS]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_COMMA]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_MINUS]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_PERIOD]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_SLASH]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_0]				 = XKEY_0;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_1]				 = XKEY_1;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_2]				 = XKEY_2;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_3]				 = XKEY_3;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_4]				 = XKEY_4;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_5]				 = XKEY_5;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_6]				 = XKEY_6;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_7]				 = XKEY_7;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_8]				 = XKEY_8;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_9]				 = XKEY_9;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_COLON]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_SEMICOLON]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_LESS]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_EQUALS]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_GREATER]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_QUESTION]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_AT]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_LEFTBRACKET]	 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_BACKSLASH]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_RIGHTBRACKET]	 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_CARET]			 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_UNDERSCORE]	 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_BACKQUOTE]		 = XKEY_NULL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_A]				 = XKEY_A;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_B]				 = XKEY_B;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_C]				 = XKEY_C;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_D]				 = XKEY_D;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_E]				 = XKEY_E;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_F]				 = XKEY_F;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_G]				 = XKEY_G;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_H]				 = XKEY_H;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_I]				 = XKEY_I;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_J]				 = XKEY_J;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_K]				 = XKEY_K;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_L]				 = XKEY_L;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_M]				 = XKEY_M;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_N]				 = XKEY_N;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_O]				 = XKEY_O;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_P]				 = XKEY_P;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_Q]				 = XKEY_Q;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_R]				 = XKEY_R;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_S]				 = XKEY_S;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_T]				 = XKEY_T;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_U]				 = XKEY_U;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_V]				 = XKEY_V;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_W]				 = XKEY_W;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_X]				 = XKEY_X;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_Y]				 = XKEY_Y;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_Z]				 = XKEY_Z;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_DELETE]		 = XKEY_DELETE;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_RIGHT]			 = XKEY_RIGHT;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_LEFT]			 = XKEY_LEFT;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_DOWN]			 = XKEY_DOWN;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_UP]			 = XKEY_UP;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_CTRL]			 = XKEY_LCONTROL;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_SHIFT]			 = XKEY_LSHIFT;
    ecs_cry_gameplay_input_key_codes[ECS_KEY_ALT]			 = XKEY_LALT;
}

bool ecs_cry_is_input_key_down(EcsKeyState *key)
{
    return key->state && key->current && !key->pressed;
}
bool ecs_cry_is_input_key_up(EcsKeyState *key)
{
    return key->state && !key->current && !key->pressed;
}

void ecs_cry_any_input_key_down(EcsKeyState *key)
{
    if (key->state) {
        key->pressed = false;
    } else {
        key->pressed = true;
    }

    key->state = true;
    key->current = true;
}

void ecs_cry_any_input_key_up(EcsKeyState *key)
{
    key->current = false;
}

void ecs_cry_any_input_key_reset(EcsKeyState *state)
{
    if (!state->current) {
        state->state = 0;
        state->pressed = 0;
    } else if (state->state) {
        state->pressed = 0;
    }
}

const KeyCodes& ecs_cry_gameplay_input_key_code(int index)
{
    return ecs_cry_gameplay_input_key_codes[index];
}
const uint8& ecs_cry_editor_input_key_code(int index)
{
    return ecs_cry_editor_input_key_codes[index];
}
