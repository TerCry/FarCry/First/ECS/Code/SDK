#include "ecs-modules.hpp"

#pragma comment(lib, "flecs.lib")
#pragma comment(lib, "flecs_util.lib")
#pragma comment(lib, "flecs_math.lib")
#pragma comment(lib, "flecs_components_transform.lib")
#pragma comment(lib, "flecs_components_geometry.lib")
#pragma comment(lib, "flecs_components_graphics.lib")
#pragma comment(lib, "flecs_components_input.lib")
#pragma comment(lib, "flecs_systems_transform.lib")

#pragma comment(lib, "flecs_components_meta.lib")

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS type-safe modules importing
/////////////////////////////////////////////////////////////////////////////////////////////
#define ECS_IMPORT_COMPONENT ecs_import_component
#define ECS_IMPORT_ENTITY ecs_import_tag

ecs_import_module(FlecsComponentsTransform)
ecs_import_module(FlecsComponentsGeometry)
ecs_import_module(FlecsComponentsGraphics)
ecs_import_module(FlecsComponentsInput)
ecs_import_module(FlecsSystemsTransform)

#undef ECS_IMPORT_COMPONENT
#undef ECS_IMPORT_ENTITY

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS type-unsafe modules importing
/////////////////////////////////////////////////////////////////////////////////////////////
#define ECS_IMPORT_COMPONENT ecs_import_type
#define ECS_IMPORT_ENTITY ecs_import_tag

ecs_import_module(FlecsComponentsMeta)

#undef ECS_IMPORT_COMPONENT
#undef ECS_IMPORT_ENTITY
//////////////////////////////////////////////////////////////////////////////////////////////
// ECS modules integration
/////////////////////////////////////////////////////////////////////////////////////////////
void ecs_cry_modules_integrate(ecs* self, int flags)
{
    // type-safe modules integration
    self->import<FlecsComponentsTransform>(flags);
    self->import<FlecsComponentsGraphics>(flags);
    self->import<FlecsComponentsGeometry>(flags);
    self->import<FlecsComponentsInput>(flags);
    self->import<FlecsSystemsTransform>(flags);
    
    // type-unsafe modules integration
    self->import<FlecsComponentsMeta>(flags);
};

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS misc
//////////////////////////////////////////////////////////////////////////////////////////////

EcsColor ecs_color_WHITE = {255, 255, 255, 255};

void ecs_mat3x3_set_identity(EcsMat3x3 *matrix) 
{
    matrix->data[0][0] = 1;
    matrix->data[0][1] = 0;
    matrix->data[0][2] = 0;
    
    matrix->data[1][0] = 0;
    matrix->data[1][1] = 1;
    matrix->data[1][2] = 0;
    
    matrix->data[2][0] = 0;
    matrix->data[2][1] = 0;
    matrix->data[2][2] = 0;
}
