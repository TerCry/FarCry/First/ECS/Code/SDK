#pragma once

#include "ecs.hpp"

#include <windows.h>

ecs_define_tag(,app_state_edit);
ecs_define_tag(,editor_entity);
ecs_define_tag(,editor_system);

struct ecs_cry_editor
{
    HWND root;
    HWND window;
    HWND perspective;
};

void ecs_cry_editor_integrate(ecs* self, int flags);
void ecs_cry_editor_launch(ecs* self);