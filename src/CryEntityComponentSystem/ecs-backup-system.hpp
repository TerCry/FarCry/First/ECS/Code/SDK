#pragma once

#include "ecs.hpp"

ecs_define_tag(,setup);

void ecs_cry_backup_integrate(ecs* self, int flags);

ecs_entity_t ecs_new_backup(ecs_world_t *world, ecs_entity_t entity);
void ecs_store_backup(ecs_world_t *world, ecs_entity_t entity);
void ecs_restore_backup(ecs_world_t *world, ecs_entity_t entity);

void ecs_cry_do_new_backup(ecs_rows_t *rows);
void ecs_cry_do_store_backup(ecs_rows_t *rows);
void ecs_cry_do_restore_backup(ecs_rows_t *rows);
