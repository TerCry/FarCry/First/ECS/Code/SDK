#pragma once

#include "ecs.hpp"

void ecs_cry_dbg_system_status(
    ecs_world_t *world,
    ecs_entity_t system,
    ecs_system_status_t status,
    void *ctx
);

void ecs_cry_dbg_entity_to_json(
    ecs_world_t *world,
    ecs_strbuf_t *buf,
    ecs_entity_t entity
);

bool ecs_cry_dbg_component_to_json(
    ecs_world_t *world, 
    ecs_strbuf_t *buf, 
    ecs_entity_t component,
    void*& ptr
);

void ecs_cry_dbg_integrate(ecs* self, int flags);