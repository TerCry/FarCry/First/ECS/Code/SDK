#pragma once

#include "ecs.hpp"

struct ecs_cry_pending_system
{
    static ecs_vector_params_t params;
    
    ecs_vector_t* self;

    static void init(ecs_rows_t* rows);

    template<size_t pending_column, size_t systems_begin, size_t systems_end>
    static void add(ecs_rows_t* rows)
    {
        ECS_COLUMN(rows, ecs_cry_pending_system, pending_system, pending_column);

        for (size_t j = systems_begin; j < systems_end && j < rows->column_count; ++j) 
        {
            ecs_entity_t system = rows->components[j];
            
            ecs_os_dbg(" - add: %s (pending-system)", ecs_get_id(rows->world, system));

            *((ecs_entity_t*)ecs_vector_add(&pending_system->self, &params)) = system;
        }
    }

    static void run(ecs_rows_t* rows);
    static void deinit(ecs_rows_t* rows);
    static void integrate(ecs* self, int flags);
};