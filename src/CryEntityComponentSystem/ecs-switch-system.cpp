#include "ecs-switch-system.hpp"
#include "ecs.hpp"

void ecs_cry_do_switch(ecs_rows_t* rows)
{
    if (rows->count == 0)
    {
        return;
    }
    ecs_entity_t domain = ecs_column_entity(rows, 1);
    ecs_entity_t entity = ecs_column_entity(rows, 2);
    ecs_entity_t target = ecs_column_entity(rows, 3);

    EcsId domain_id = ecs_get_id(rows->world, domain);
    
    for (size_t i = 0; i < rows->count; ++i)
    {
        ecs_entity_t	state = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, state);
        
        if (target == state)
        {
            ecs_os_dbg(" + switch: %s (%s)", id, domain_id);
        }
        else
        {
            ecs_os_dbg(" - switch: %s (%s)", id, domain_id);

            ecs_type_t type = ecs_type_from_entity(rows->world, state);

            _ecs_remove(rows->world, entity, type);
        }
    }
}
