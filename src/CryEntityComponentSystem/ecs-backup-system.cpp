#include "ecs-backup-system.hpp"

void ecs_cry_backup_integrate(ecs* self, int flags)
{
    self->new_tag<setup>("setup");
}

ecs_entity_t ecs_new_backup(ecs_world_t *world, ecs_entity_t entity)
{
    ecs_type_t type = ecs_get_type(world, entity);
    ecs_entity_t backup = _ecs_new(world, type);
    
    ecs_inherit(world, entity, backup);
    ecs_inherit(world, backup, entity);

    ecs_add(world, backup, EcsDisabled);
    //ecs_remove(world, backup, EcsId);

    return backup;
}

void ecs_store_backup(ecs_world_t *world, ecs_entity_t entity)
{
    ecs_type_t type = ecs_get_type(world, entity);
    ecs_type_index_t type_index = 0;
    
    if (ecs_type_next_index(type, ECS_INSTANCEOF, &type_index))
    { 
        ecs_entity_t *array = (ecs_entity_t *)ecs_vector_first(type);

        ecs_entity_t backup = array[type_index] & ECS_ENTITY_MASK;
        
        ecs_type_t backup_type = ecs_get_type(world, backup);
        
        ecs_assert(backup_type != NULL, ECS_INVALID_TYPE_EXPRESSION ,NULL);

        _ecs_remove(world, backup, backup_type);
        _ecs_add(world, backup, backup_type);
    }
}

void ecs_restore_backup(ecs_world_t *world, ecs_entity_t entity)
{
    ecs_type_t type = ecs_get_type(world, entity);

    _ecs_remove(world, entity, type);
    _ecs_add(world, entity, type);
}

void ecs_cry_do_new_backup(ecs_rows_t *rows)
{
    ecs* self = ecs::get(rows->world);

    for (uint32_t i = 0; i < rows->count; i ++) {
        
        ecs_entity_t	e = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, e);

        ecs_os_dbg(" - backup: %s (new)", id);

        self->remove<setup>(e);

        ecs_new_backup(self->get_world(), e);
    }
}

void ecs_cry_do_store_backup(ecs_rows_t *rows)
{
    ecs_world_t *world = rows->world;

    for (uint32_t i = 0; i < rows->count; i ++) {
        
        ecs_entity_t	e = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, e);

        ecs_os_dbg(" - backup: %s (store)", id);

        ecs_store_backup(world, e);
    }
}
void ecs_cry_do_restore_backup(ecs_rows_t *rows)
{
    ecs_world_t *world = rows->world;

    for (uint32_t i = 0; i < rows->count; i ++) {
        
        ecs_entity_t	e = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, e);

        ecs_os_dbg(" - backup: %s (restore)", id);

        ecs_restore_backup(world, e);
    }
}

