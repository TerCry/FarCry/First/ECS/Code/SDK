#include "ecs.hpp"


//////////////////////////////////////////////////////////////////////////////////////////////
// ECS C++ implementation
//////////////////////////////////////////////////////////////////////////////////////////////
namespace _ecs {
    ecs_vector_params_t ptr_builder::params = {0, 0, 0, sizeof(ptr_handle)};
    ecs_vector_params_t type_builder::params = {0, 0, 0, sizeof(ecs_entity_t)};
    ecs_vector_params_t typesafe_handles::params = {0, 0, 0, sizeof(handle)};

    
    type_builder::type_builder(typesafe_handles* handles) :
        handles(handles),
        array(ecs_vector_new(&params, 0))
    { }
    type_builder::~type_builder() {
        ecs_vector_free(array);
    }

    type_builder& type_builder::add(ecs_entity_t entity) {
        *static_cast<ecs_entity_t*>(ecs_vector_add(&array, &params)) = entity;

        return *this;
    }
    type_builder& type_builder::add(const ptr_handle* begin, const ptr_handle* end) {
        for (const ptr_handle* it = begin; it != end; ++it) {
            *static_cast<ecs_entity_t*>(ecs_vector_add(&array, &params)) = it->component->entity;
        }

        return *this;
    }

    type_builder& type_builder::childof(ecs_entity_t entity) {
        return add(ECS_CHILDOF | entity);
    }
    type_builder& type_builder::instanceof(ecs_entity_t entity) {
        return add(ECS_INSTANCEOF | entity);
    }

    type_builder& type_builder::prefab() {
        return add(EEcsPrefab);
    }
    type_builder& type_builder::hidden() {
        return add(EEcsHidden);
    }
    type_builder& type_builder::disabled() {
        return add(EEcsDisabled);
    }

    ecs_type_t type_builder::build() {
        ecs_type_t type = 0;
        uint32_t count = ecs_vector_count(array);
        
        if (count > 0) {
            ecs_entity_t* first = (ecs_entity_t*)ecs_vector_first(array);
            type = ecs_type_find(handles->world, first, count);

            ecs_vector_clear(array);
        }

        return type;
    }
    ecs_entity_t type_builder::build(EcsId id) {
        ecs_type_t type = build();
        ecs_entity_t entity = _ecs_new(handles->world, TEcsId);
        
        struct {
            ecs_type_t type;   
            ecs_type_t resolved;  
        } result = { type, type };

        _ecs_set_ptr(handles->world, entity, EEcsId, sizeof(id), &id);
        _ecs_set_ptr(handles->world, entity, EEcsTypeComponent, sizeof(result), &result);

        return entity;
    }

    
    ptr_builder::ptr_builder(typesafe_handles* handles) :
        handles(handles),
        array(ecs_vector_new(&params, 0))
    { }
    ptr_builder::~ptr_builder() {
        ecs_vector_free(array);
    }

    ptr_handle* ptr_builder::begin() {
        return (ptr_handle*)ecs_vector_first(array);
    }
    ptr_handle* ptr_builder::end() {
        return begin() + ecs_vector_count(array);
    }

    const ptr_handle* ptr_builder::begin() const {
        return (const ptr_handle*)ecs_vector_first(array);
    }
    const ptr_handle* ptr_builder::end() const {
        return begin() + ecs_vector_count(array);
    }


    ptr_builder& ptr_builder::add(const ptr_handle& handle) {
        *static_cast<ptr_handle*>(ecs_vector_add(&array, &params)) = handle;
        
        return *this;
    }

    ptr_builder& ptr_builder::set(ecs_entity_t entity) {
        uint32_t count = ecs_vector_count(array);
        
        if (count > 0) {
            const ptr_handle* it = begin();
            const ptr_handle* it_end = end();

            for (; it != it_end; ++it) {
                _ecs_set_ptr(handles->world, entity, it->component->entity, it->size, it->ptr);
            }
        }
    
        return *this;
    }
    ptr_builder& ptr_builder::get(ecs_entity_t entity) {
        uint32_t count = ecs_vector_count(array);
        
        if (count > 0) {
            ptr_handle* it = begin();
            ptr_handle* it_end = end();

            for (; it != it_end; ++it) {
                it->ptr = _ecs_get_ptr(handles->world, entity, it->component->type);
            }
        }
    
        return *this;
    }

    ptr_builder& ptr_builder::clear() {
        ecs_vector_clear(array);
        return *this;
    }

    
    entity_builder::entity_builder(typesafe_handles* handles) :
        ptr(handles),
        type(handles)
    { }
    entity_builder::~entity_builder()
    { }

    entity_builder& entity_builder::add(ecs_entity_t entity) {
        type.add(entity);

        return *this;
    }

    entity_builder& entity_builder::childof(ecs_entity_t entity) {
        type.childof(entity);
        
        return *this;
    }
    entity_builder& entity_builder::instanceof(ecs_entity_t entity) {
        type.instanceof(entity);
        
        return *this;
    }

    entity_builder& entity_builder::prefab() {
        type.prefab();
        
        return *this;
    }
    entity_builder& entity_builder::hidden() {
        type.hidden();
        
        return *this;
    }
    entity_builder& entity_builder::disabled() {
        type.disabled();
        
        return *this;
    }

    ecs_entity_t entity_builder::build(EcsId id) {
        ecs_component::EcsId wrapped_id = id;

        ptr.add(&wrapped_id);

        type.add(ptr.begin(), ptr.end());

        ecs_entity_t entity = _ecs_new(type.handles->world, type.build()); 

        ptr.set(entity);
        ptr.clear();

        return entity;
    }


    system_builder::system_builder(typesafe_handles* handles) :
        handles(handles)
    {
        memset(&signature, 0, sizeof(signature));
    }

    system_builder& system_builder::optional() {
        return str("?");
    }

    system_builder& system_builder::and() {
        return str(",");
    }
    
    system_builder& system_builder::not() {
        return str("!");
    }
    
    system_builder& system_builder::or() {
        return str("|");
    }
    
    system_builder& system_builder::typeof(ecs_entity_t entity) {
        EcsId eid = ecs_get_id(handles->world, entity);

        return str(eid);
    }

    system_builder& system_builder::in(ecs_entity_t entity) {
        return src("[int] ", entity);
    }

    system_builder& system_builder::out(ecs_entity_t entity) {
        return src("[out] ", entity);
    }

    system_builder& system_builder::self(ecs_entity_t entity) {
        return src("", entity);
    }

    system_builder& system_builder::owned(ecs_entity_t entity) {
        return src("OWNED.", entity);
    }

    system_builder& system_builder::shared(ecs_entity_t entity) {
        return src("SHARED.", entity);
    }

    system_builder& system_builder::nothing(ecs_entity_t entity) {
        return src(".", entity);
    }

    system_builder& system_builder::container(ecs_entity_t entity) {
        return src("CONTAINER.", entity);
    }

    system_builder& system_builder::cascade(ecs_entity_t entity) {
        return src("CASCADE.", entity);
    }

    system_builder& system_builder::system(ecs_entity_t entity) {
        return src("SYSTEM.", entity);
    }

    system_builder& system_builder::singleton(ecs_entity_t entity) {
        return src("$.", entity);
    }

    system_builder& system_builder::entity(ecs_entity_t entity, ecs_entity_t component) {
        return ref(entity, component);
    }  

    ecs_entity_t system_builder::build(EcsId id, EcsSystemKind kind, ecs_system_action_t action) {
        char* sig = build();
        
        ecs_entity_t sys = ecs_new_system(handles->world, id, kind, sig, action);
        
        //FIXME: crash
        //free(sig);

        return sys;
    }

    system_builder& system_builder::str(const char* str) {
        ecs_strbuf_appendstr(&signature, str);

        return *this;
    }
    system_builder& system_builder::src(const char* prefix, ecs_entity_t entity) {
        EcsId eid = ecs_get_id(handles->world, entity);

        ecs_strbuf_append(&signature, ",%s%s", prefix, eid);

        return *this;
    }
    system_builder& system_builder::ref(ecs_entity_t entity, ecs_entity_t component) {
        EcsId eid = ecs_get_id(handles->world, entity);
        EcsId cid = ecs_get_id(handles->world, component);

        ecs_strbuf_append(&signature, ",%s.%s", eid, cid);
    
        return *this;
    }

    char* system_builder::build() {
        char* sig = ecs_strbuf_get(&signature);

        if (sig && sig[0]) {
            sig[0] = ' ';
        }
        
        return sig;
    }
}

void ecs::ctor(ecs_world_t* world, bool owned) {
    if (world == NULL)
    {
        owned = true;
        world = ecs_init();
    }

    handles.world = world;

    this->owned = owned;
    this->world = world;
    
    context = ecs_get_context(world);

    ecs_set_context(world, this);
    
    import<ecs_world_t>(0);
}

ecs::ecs(int argc, char *argv[]) {
    ctor(ecs_init_w_args(argc, argv), true);
}
ecs::ecs(ecs_world_t* world, bool owned) {
    ctor(world, owned);
}

ecs::~ecs() {
    ecs_set_context(world, context);


    if (owned)
    {
        ecs_fini(world);
    }
}
ecs* ecs::get(ecs_world_t* world) {
    return (ecs*)ecs_get_context(world);
}

ecs_world_t* ecs::get_world() {
    return world;
}

_ecs::type_builder ecs::new_type_builder() {
    return _ecs::type_builder(&handles);
}
_ecs::ptr_builder ecs::new_ptr_builder() {
    return _ecs::ptr_builder(&handles);
}
_ecs::entity_builder ecs::new_entity_builder() {
    return _ecs::entity_builder(&handles);
}
_ecs::system_builder ecs::new_system_builder() {
    return _ecs::system_builder(&handles);
}

ecs_entity_t ecs::new_entity(EcsId id, ecs_type_t type) {
    ecs_entity_t entity = _ecs_new(world, type); 
    
    set<ecs_component::EcsId>(entity, id);

    return entity;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS type-safe core importing
//////////////////////////////////////////////////////////////////////////////////////////////
#define ECS_IMPORT_COMPONENT(_, id) handles.set<ecs_component::id>(ecs_entity(id));
#define ECS_IMPORT_ENTITY(_, id) handles.set<ecs_component::id>(ecs_entity(id));

template<> void ecs::import<ecs_world_t>(int flags) {
    FlecsImportHandles(void);
}
#undef ECS_IMPORT_COMPONENT
#undef ECS_IMPORT_ENTITY



//////////////////////////////////////////////////////////////////////////////////////////////
// ECS new api implementation
//////////////////////////////////////////////////////////////////////////////////////////////
bool ecs_type_next_index(ecs_type_t type, ecs_entity_t mask, ecs_type_index_t* index)
{
    ecs_entity_t *array = (ecs_entity_t *)ecs_vector_first(type);
    ecs_type_index_t count = ecs_vector_count(type);

    for (ecs_type_index_t i = count - 1 - index[0]; i >= 0; i --) {
        if (array[i] & mask) {
            index[0] = i;
            return true;
        }
    }
    return false;
}


