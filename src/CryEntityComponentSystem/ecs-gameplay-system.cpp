#include "ecs-gameplay-system.hpp"
#include "ecs-modules.hpp"
#include "ecs-backup-system.hpp"
#include "ecs-switch-system.hpp"
#include "ecs-input-system.hpp"
#include "ecs-disabling-system.hpp"
#include "ecs-pending-system.hpp"
#include "ecs-app-system.hpp"

#include <ISystem.h>
#include <IInput.h>

void ecs_cry_do_gameplay_init(ecs_rows_t *rows)
{

}

void ecs_cry_do_gameplay_deinit(ecs_rows_t *rows) 
{
}


void ecs_cry_do_gameplay_input(ecs_rows_t* rows)
{ 
    ecs_world_t *world = rows->world;
    EcsInput *input = ecs_column(rows, EcsInput, 1);
    ECS_COLUMN(rows, EcsCanvas2D, canvas, 2);

    GetISystem()->GetIInput()->Update(1);

    IKeyboard* keyboard = GetISystem()->GetIInput()->GetIKeyboard();
    IMouse* mouse = GetISystem()->GetIInput()->GetIMouse();
    for (int i = 0; i < rows->count; i ++) {
        {
            for (int k = 0; k < 128; k ++) {
                const KeyCodes& key_code = ecs_cry_gameplay_input_key_code(k);
                
                ecs_cry_any_input_key_reset(&input[i].keys[k]);

                if (key_code == XKEY_NULL) {
                    //skip unknown key code
                } else if (keyboard->KeyDown(key_code)) {
                    ecs_cry_any_input_key_down(&input[i].keys[k]);
                } else if (keyboard->KeyReleased(key_code)) {
                    ecs_cry_any_input_key_up(&input[i].keys[k]);
                }
            }
        } {
            ecs_cry_any_input_key_reset(&input[i].mouse.left);
            ecs_cry_any_input_key_reset(&input[i].mouse.right);

            if (mouse->MouseDown(XKEY_MOUSE1)) {
                ecs_cry_any_input_key_down(&input[i].mouse.left);
            } else if (mouse->MouseReleased(XKEY_MOUSE1)) {
                ecs_cry_any_input_key_up(&input[i].mouse.left);
            }

            if (mouse->MouseDown(XKEY_MOUSE2)) {
                ecs_cry_any_input_key_down(&input[i].mouse.left);
            } else if (mouse->MouseReleased(XKEY_MOUSE1)) {
                ecs_cry_any_input_key_up(&input[i].mouse.left);
            }

            input[i].mouse.wnd.x = mouse->GetVScreenX();
            input[i].mouse.wnd.y = mouse->GetVScreenY();
            input[i].mouse.rel.x = mouse->GetDeltaX();
            input[i].mouse.rel.y = mouse->GetDeltaY();


            input[i].mouse.view.x = 
                input[i].mouse.wnd.x * ((float)canvas->viewport.width / (float)canvas->window.width) + canvas->viewport.x;
            input[i].mouse.view.y = 
                input[i].mouse.wnd.y * ((float)canvas->viewport.width / (float)canvas->window.width) + canvas->viewport.y;

            
            input[i].mouse.scroll.x = 0;
            input[i].mouse.scroll.y = mouse->GetDeltaZ();
        }
    }
}

void ecs_cry_gameplay_integrate(ecs* self, int flags)
{
    self->new_tag<gameplay_entity>("gameplay_entity");
    self->new_tag<gameplay_system>("gameplay_system");
    
    self->new_component<ecs_cry_gameplay>("ecs_cry_gameplay");

    self->add<app_state>(self->new_tag<app_state_play>("app_state_play")->entity);

    ecs_entity_t gameplay_state_switch = self->new_system_builder()
        .self<app_state>()
        .nothing(ECS_SINGLETON)
        .nothing<app_state_play>()
        .build("gameplay_state_switch", EcsManual, &ecs_cry_do_switch)
    ;

    ecs_entity_t gameplay_disable_systems = self->new_system_builder()
        .self<gameplay_system>()
        .build("gameplay_disable_systems", EcsManual, &ecs_cry_do_disable_systems)
    ;
    
    ecs_entity_t gameplay_enable_systems = self->new_system_builder()
        .self<gameplay_system>()
        .build("gameplay_enable_systems", EcsManual, &ecs_cry_do_enable_systems)
    ;

    ecs_entity_t gameplay_setup_entities = self->new_system_builder()
        .self<setup>()
        .self<gameplay_entity>()
        .build("gameplay_setup_entities", EcsManual, &ecs_cry_do_new_backup)
    ;
    
    ecs_entity_t gameplay_store_entities = self->new_system_builder()
        .self<gameplay_entity>()
        .build("gameplay_store_entities", EcsManual, &ecs_cry_do_store_backup)
    ;
    
    ecs_entity_t gameplay_restore_entities = self->new_system_builder()
        .self<gameplay_entity>()
        .build("gameplay_restore_entities", EcsManual, &ecs_cry_do_restore_backup)
    ;
    
    self->new_system_builder()
        .self<ecs_cry_gameplay>()
        .system<ecs_component::EcsHidden>()
        .build("gameplay_init", EcsOnAdd, &ecs_cry_do_gameplay_init)
    ;

    self->new_system_builder()
        .self<ecs_cry_gameplay>()
        .singleton<ecs_cry_pending_system>()
        .nothing(gameplay_setup_entities)
        .nothing(gameplay_disable_systems)
        .build("gameplay_setup", EcsOnAdd, &ecs_cry_pending_system::add<2,2,-1>)
    ;
    
    self->new_system_builder()
        .self<ecs_cry_gameplay>()
        .system<ecs_component::EcsHidden>()
        .build("gameplay_deinit", EcsOnRemove, &ecs_cry_do_gameplay_deinit)
    ;
    
    self->new_system_builder()
        .self<app_state_play>()
        .singleton<ecs_cry_pending_system>()
        .nothing(gameplay_state_switch)
        .nothing(gameplay_setup_entities)
        .nothing(gameplay_store_entities)
        .nothing(gameplay_enable_systems) 
        .build("gameplay_activate", EcsOnAdd, &ecs_cry_pending_system::add<2,2,-1>)
    ;
    
    self->new_system_builder()
        .self<app_state_play>()
        .singleton<ecs_cry_pending_system>()
        .nothing(gameplay_restore_entities)
        .nothing(gameplay_disable_systems)
        .build("gameplay_deactive", EcsOnRemove, &ecs_cry_pending_system::add<2,2,-1>)
    ;

    self->new_system_builder()
        .self<ecs_component::EcsInput>()
        .singleton<ecs_component::EcsCanvas2D>()
        .system<gameplay_system>()
        .system<ecs_component::EcsHidden>()
        .build("gameplay_input", EcsOnLoad, &ecs_cry_do_gameplay_input)
    ;
    
    ecs_cry_gameplay_input_key_setup();
}
void ecs_cry_game_launch(ecs* self)
{

}

