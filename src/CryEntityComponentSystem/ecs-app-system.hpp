#pragma once

#include "ecs.hpp"

ecs_define_tag(,app_state);


void ecs_cry_app_integrate(ecs* self, int flags);