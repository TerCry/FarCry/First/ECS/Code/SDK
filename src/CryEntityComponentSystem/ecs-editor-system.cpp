#include "ecs-editor-system.hpp"
#include "ecs-modules.hpp"
#include "ecs-backup-system.hpp"
#include "ecs-switch-system.hpp"
#include "ecs-input-system.hpp"
#include "ecs-disabling-system.hpp"
#include "ecs-pending-system.hpp"
#include "ecs-app-system.hpp"
#include "ecs-dbg-system.hpp"

BOOL CALLBACK ecs_cry_editor_enum(HWND child, LPARAM lParam) 
{	
    HWND parent = GetParent(child);

    char parent_name[128];
    char parent_class[128];

    char child_name[128];
    char child_class[128];

    GetWindowTextA(parent, parent_name, 128);
    GetClassNameA(parent, parent_class, 128);

    GetWindowTextA(child, child_name, 128);
    GetClassNameA(child, child_class, 128);

    return TRUE;
}

void ecs_cry_do_editor_init(ecs_rows_t *rows) {
    ecs_cry_editor *editor = ecs_column(rows, ecs_cry_editor, 1);
    
    editor->root = FindWindowExA(0, NULL, "CryEditorClass", NULL);
    editor->window = FindWindowExA(editor->root, NULL, "AfxWnd70", "");
    editor->perspective = FindWindowExA(editor->window, NULL, "AfxFrameOrView70", "Perspective");

    //EnumChildWindows(editor->root, ecs_cry_editor_enum, 0);
}

void ecs_cry_do_editor_cursor(ecs_rows_t *rows) {
    
    ECS_COLUMN(rows, EcsInput, input, 1);
    ECS_COLUMN(rows, EcsPosition2D, position, 2);
    
    for (int i = 0; i < rows->count; i ++) {
        ecs_entity_t e = rows->entities[i];

        position[i].x = input->mouse.wnd.x;
        position[i].y = input->mouse.wnd.y;
        
        if (ecs_cry_is_input_key_up(&input->mouse.left))
        { 
            ecs_strbuf_t json;
            memset(&json, 0, sizeof(json));

            EcsId		 eid = ecs_get_id(rows->world, e);

            ecs_os_dbg(" > sample[0x%p]: %f (x), %f (y), %s (entity)", &position[i], position[i].x, position[i].y, eid);

            ecs_cry_dbg_entity_to_json(rows->world, &json, e);

            char* str = ecs_strbuf_get(&json);

            ecs_os_dbg(str);

            //FIXME
            //free(str);
        }
    }
}

void ecs_cry_do_editor_deinit(ecs_rows_t *rows) {
    ecs_cry_editor *editor = ecs_column(rows, ecs_cry_editor, 1);
    int i;
    for (i = 0; i < rows->count; i ++) {

    }
}

void ecs_cry_do_editor_input(ecs_rows_t* rows)
{
    POINT cursor;
    ecs_world_t *world = rows->world;
    EcsInput *input = ecs_column(rows, EcsInput, 1);
    
    ECS_COLUMN(rows, EcsCanvas2D, canvas, 2);
    ECS_COLUMN_TYPE(rows, ecs_cry_editor, 3);

    ecs_cry_editor *editor = (ecs_cry_editor*)ecs_get_singleton_ptr(world, ecs_cry_editor);


    if (GetCursorPos(&cursor))
    {
        if (ScreenToClient(editor->perspective, &cursor))
        {
            for (int i = 0; i < rows->count; i ++) {
                const bool w = cursor.x >= 0 && cursor.x <= canvas->window_actual.width;
                const bool h = cursor.y >= 0 && cursor.y <= canvas->window_actual.height;

                if (w && h)
                {
                    const int16 left_state = GetAsyncKeyState(VK_LBUTTON);
                    const int16 right_state = GetAsyncKeyState(VK_RBUTTON);

                    for (int k = 0; k < 128; k ++) {
                        const uint8& key_code = ecs_cry_editor_input_key_code(k);

                        ecs_cry_any_input_key_reset(&input[i].keys[k]);

                        if (key_code == 0xFF) {
                            //skip unknown key code
                        } 
                        else 
                        {
                            const int16	 key_state = GetAsyncKeyState(key_code);

                            if ((key_state & 0x8000) != 0) {
                                ecs_cry_any_input_key_down(&input[i].keys[k]);
                            } else if (input[i].keys[k].current) {
                                ecs_cry_any_input_key_up(&input[i].keys[k]);
                            }
                        }
                    }

                    ecs_cry_any_input_key_reset(&input[i].mouse.left);
                    ecs_cry_any_input_key_reset(&input[i].mouse.right);
                    

                    if ((left_state & 0x8000) != 0)
                    {
                        ecs_cry_any_input_key_down(&input[i].mouse.left);
                    }
                    else if (input[i].mouse.left.current)
                    {
                        ecs_cry_any_input_key_up(&input[i].mouse.left);
                    }

                    if ((right_state & 0x8000) != 0)
                    {
                        ecs_cry_any_input_key_down(&input[i].mouse.right);
                    }
                    else if (input[i].mouse.right.current)
                    {
                        ecs_cry_any_input_key_up(&input[i].mouse.right);
                    }
                    
                    //TODO: better
                    const float x = float(cursor.x) - float(canvas->window_actual.width) * 0.5f;
                    const float y = float(cursor.y) - float(canvas->window_actual.height) * 0.5f;

                    input[i].mouse.rel.x = input[i].mouse.wnd.x - x;
                    input[i].mouse.rel.y = input[i].mouse.wnd.y - y;
                    input[i].mouse.wnd.x = x;
                    input[i].mouse.wnd.y = y;

                    input[i].mouse.view.x = 
                        input[i].mouse.wnd.x * ((float)canvas->viewport.width / (float)canvas->window.width) + canvas->viewport.x;
                    input[i].mouse.view.y = 
                        input[i].mouse.wnd.y * ((float)canvas->viewport.width / (float)canvas->window.width) + canvas->viewport.y;

                }
            }
        }
    }
}

void ecs_cry_editor_integrate(ecs* self, int flags)
{
    self->new_tag<editor_entity>("editor_entity");
    self->new_tag<editor_system>("editor_system");
    
    self->new_component<ecs_cry_editor>("ecs_cry_editor");
    
    self->add<app_state>(self->new_tag<app_state_edit>("app_state_edit")->entity);
    
    ecs_entity_t editor_state_switch = self->new_system_builder()
        .self<app_state>()
        .nothing(ECS_SINGLETON)
        .nothing<app_state_edit>()
        .build("editor_state_switch", EcsManual, &ecs_cry_do_switch)
    ;

    ecs_entity_t editor_disable_entities = self->new_system_builder()
        .self<editor_entity>()
        .build("editor_disable_entities", EcsManual, &ecs_cry_do_disable_entities)
    ;

    ecs_entity_t editor_enable_entities = self->new_system_builder()
        .self<editor_entity>()
        .self<ecs_component::EcsDisabled>()
        .build("editor_enable_entities", EcsManual, &ecs_cry_do_enable_entities)
    ;
    
    ecs_entity_t editor_disable_systems = self->new_system_builder()
        .self<editor_system>()
        .build("editor_disable_systems", EcsManual, &ecs_cry_do_disable_systems)
    ;
    
    ecs_entity_t editor_enable_systems = self->new_system_builder()
        .self<editor_system>()
        .build("editor_enable_systems", EcsManual, &ecs_cry_do_enable_systems)
    ;
    
    self->new_system_builder()
        .self<ecs_cry_editor>()
        .system<ecs_component::EcsHidden>()
        .build("editor_init", EcsOnAdd, &ecs_cry_do_editor_init)
    ;

    self->new_system_builder()
        .self<ecs_cry_editor>()
        .singleton<ecs_cry_pending_system>()
        .nothing(editor_disable_systems)
        .nothing(editor_disable_entities)
        .build("editor_setup", EcsOnAdd, &ecs_cry_pending_system::add<2,2,-1>)
    ;

    self->new_system_builder()
        .self<ecs_cry_editor>()
        .system<ecs_component::EcsHidden>()
        .build("editor_deinit", EcsOnRemove, &ecs_cry_do_editor_deinit)
    ;

    self->new_system_builder()
        .self<app_state_edit>()
        .singleton<ecs_cry_pending_system>()
        .nothing(editor_state_switch)
        .nothing(editor_enable_systems)
        .nothing(editor_enable_entities)
        .build("editor_activate", EcsOnAdd, &ecs_cry_pending_system::add<2,2,-1>)
    ;

    self->new_system_builder()
        .self<app_state_edit>()
        .singleton<ecs_cry_pending_system>()
        .nothing(editor_disable_systems)
        .nothing(editor_disable_entities)
        .build("editor_deactive", EcsOnRemove, &ecs_cry_pending_system::add<2,2,-1>)
    ;

    self->new_system_builder()
        .self<ecs_component::EcsInput>()
        .singleton<ecs_component::EcsCanvas2D>()
        .nothing<ecs_cry_editor>()
        .nothing<editor_system>()
        .nothing<ecs_component::EcsHidden>()
        .build("editor_input", EcsOnLoad, &ecs_cry_do_editor_input)
    ;

    ecs_cry_editor_input_key_setup();
}


void ecs_cry_editor_launch(ecs* self)
{
    /* Initialize cursor */
    ecs_component::EcsPosition2D pos;
    ecs_component::EcsRectangle rect;
    ecs_component::EcsColor col;
    ecs_component::EcsLineColor lcol;
    
    ecs_vec2_set(pos, 0.0f, 0.0f);

    rect->width = 10;
    rect->height = 10;
    
    col->r = 255;
    col->g = 255;
    col->b = 255;
    col->a = 64;

    lcol->r = 255;
    lcol->g = 255;
    lcol->b = 255;
    lcol->a = 128;

    ecs_entity_t cursor = self->new_entity_builder()
        .use(&pos)
        .use(&rect)
        .use(&col)
        .use(&lcol)
        .add<ecs_component::EcsInput>()
        .add<editor_entity>()
        .build("Cursor")
    ;

    self->new_system_builder()
        .self<ecs_component::EcsInput>()
        .self<ecs_component::EcsPosition2D>()
        .self<editor_entity>()
        .system<editor_system>()
        .build("ecs_cry_do_editor_cursor", EcsOnUpdate, &ecs_cry_do_editor_cursor)
    ;
    
    self->add<ecs_cry_editor>(ECS_SINGLETON);
}
