#include "ecs-pending-system.hpp"

ecs_vector_params_t ecs_cry_pending_system::params = {0, 0, 0, sizeof(ecs_entity_t)};

void ecs_cry_pending_system::init(ecs_rows_t* rows)
{
    ECS_COLUMN(rows, ecs_cry_pending_system, pending_system, 1);

    for (uint32_t i = 0; i < rows->count; i ++) {
        pending_system[i].self = ecs_vector_new(&params, 0);
    }
}

void ecs_cry_pending_system::run(ecs_rows_t* rows)
{
    ECS_COLUMN(rows, ecs_cry_pending_system, pending_system, 1);

    for (size_t i = 0; i < rows->count; ++i) 
    {
        size_t count = ecs_vector_count(pending_system[i].self);

        if (count > 0)
        {
            do
            {
                ecs_vector_t* copy = ecs_vector_copy(pending_system[i].self, &params);
                ecs_vector_clear(pending_system[i].self);

                ecs_entity_t* systems = (ecs_entity_t*)ecs_vector_first(copy);

                for (size_t j = 0; j < count; ++j)
                {
                    ecs_entity_t	system = systems[j]; 
                    EcsId			system_id = ecs_get_id(rows->world, system);
                
                    ecs_os_dbg(" - run: %s (pending-system)", system_id);

                    ecs_run(rows->world, system, 0, 0);
                }

                ecs_vector_free(copy);

                count = ecs_vector_count(pending_system[i].self);

            } while (count > 0);
        }
    }
}

void ecs_cry_pending_system::deinit(ecs_rows_t* rows)
{
    ECS_COLUMN(rows, ecs_cry_pending_system, pending_system, 1);

    for (uint32_t i = 0; i < rows->count; i ++) {
        ecs_vector_free(pending_system[i].self);
    }
}


void ecs_cry_pending_system::integrate(ecs* self, int flags)
{
    self->new_component<ecs_cry_pending_system>("ecs_cry_pending_system");
    
    self->new_system_builder()
        .self<ecs_cry_pending_system>()
        .build("ecs_cry_pending_system::init", EcsOnAdd, &init)
    ;
    self->new_system_builder()
        .self<ecs_cry_pending_system>()
        .build("ecs_cry_pending_system::deinit", EcsOnRemove, &deinit)
    ;
    self->new_system_builder()
        .self<ecs_cry_pending_system>()
        .build("ecs_cry_pending_system::run", EcsOnLoad, &run)
    ;
}
