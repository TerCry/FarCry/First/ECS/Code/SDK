#include "ecs-disabling-system.hpp"
#include "ecs.hpp"


//////////////////////////////////////////////////////////////////////////////////////////////
// ECS disabling systems implementation
//////////////////////////////////////////////////////////////////////////////////////////////
void ecs_cry_do_disable_systems(ecs_rows_t *rows)
{
    for (uint32_t i = 0; i < rows->count; i ++) {

        ecs_entity_t	e = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, e);

        ecs_os_dbg(" - disable: %s (system)", id);
        
        ecs_enable(rows->world, e, false);
    }
}
void ecs_cry_do_enable_systems(ecs_rows_t *rows)
{
    for (uint32_t i = 0; i < rows->count; i ++) {

        ecs_entity_t	e = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, e);

        ecs_os_dbg(" - enable: %s (system)", id);

        ecs_enable(rows->world, e, true);		
    }
}

void ecs_cry_do_disable_entities(ecs_rows_t *rows)
{
    for (uint32_t i = 0; i < rows->count; i ++) {

        ecs_entity_t	e = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, e);

        ecs_os_dbg(" - disable: %s (entity)", id);

        ecs_add(rows->world, e, EcsDisabled);		
    }
}
void ecs_cry_do_enable_entities(ecs_rows_t *rows)
{
    for (uint32_t i = 0; i < rows->count; i ++) {
        
        ecs_entity_t	e = rows->entities[i];
        EcsId			id = ecs_get_id(rows->world, e);

        ecs_os_dbg(" - enable: %s (entity)", id);

        ecs_remove(rows->world, e, EcsDisabled);		
    }
}