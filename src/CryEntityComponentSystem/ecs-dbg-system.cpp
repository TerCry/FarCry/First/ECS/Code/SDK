#include "ecs-dbg-system.hpp"
#include "ecs-modules.hpp"

#include <flecs/util/dbg.h>
#include <vector>
#include <algorithm>


EcsId EcsMetaPrimitiveIds[] = {
    "bool",
    "char",
    "uint8_t",
    "uint16_t",
    "uint32_t",
    "uint64_t",
    "int8_t",
    "int16_t",
    "int32_t",
    "int64_t",
    "intptr_t",
    "float",
    "double",
    "ecs_string_t",
    "ecs_entity_t"
};

const char* EcsMetaPrimitiveName[] = {
    "Bool",
    "Char",
    "Byte",
    "U8",
    "U16",
    "U32",
    "U64",
    "I8",
    "I16",
    "I32",
    "I64",
    "F32",
    "F64",
    "Word",
    "String",
    "Entity"
};


const char* EcsMetaTypeName[] = {
    "Primitive",
    "Bitmask",
    "Enum",
    "Struct",
    "Array",
    "Vector",
    "Map"
};
///////////////////////////////////////////////////////////////////////////////
// Include files 
//

#include <set>
#include <windows.h>
#include <tchar.h>

#include <stdio.h>


///////////////////////////////////////////////////////////////////////////////
// Directives 
//

#pragma comment( lib, "dbghelp.lib" )


///////////////////////////////////////////////////////////////////////////////
// Declarations 
//

///////////////////////////////////////////////////////////////////////////////
// Include files 
//

#define _NO_CVCONST_H
#include <dbghelp.h>


///////////////////////////////////////////////////////////////////////////////
// Types from Cvconst.h (DIA SDK) 
//

// values found in SYMBOL_INFO.Tag
//
// This was taken from cvconst.h and should
// not override any values found there.
//
// #define _NO_CVCONST_H_ if you don't
// have access to that file...

#ifdef _NO_CVCONST_H


// DIA enums

enum SymTagEnumType
{
    SymTagNull,
    SymTagExe,
    SymTagCompiland,
    SymTagCompilandDetails,
    SymTagCompilandEnv,
    SymTagFunction,
    SymTagBlock,
    SymTagData,
    SymTagAnnotation,
    SymTagLabel,
    SymTagPublicSymbol,
    SymTagUDT,
    SymTagEnum,
    SymTagFunctionType,
    SymTagPointerType,
    SymTagArrayType,
    SymTagBaseType,
    SymTagTypedef,
    SymTagBaseClass,
    SymTagFriend,
    SymTagFunctionArgType,
    SymTagFuncDebugStart,
    SymTagFuncDebugEnd,
    SymTagUsingNamespace,
    SymTagVTableShape,
    SymTagVTable,
    SymTagCustom,
    SymTagThunk,
    SymTagCustomType,
    SymTagManagedType,
    SymTagDimension,
    SymTagCallSite,
    SymTagInlineSite,
    SymTagBaseInterface,
    SymTagVectorType,
    SymTagMatrixType,
    SymTagHLSLType,
    SymTagCaller,
    SymTagCallee,
    SymTagExport,
    SymTagHeapAllocationSite,
    SymTagCoffGroup,
    SymTagMax
};

const char* SymTagEnumName[] =
{
    "Null",
    "Exe",
    "Compiland",
    "CompilandDetails",
    "CompilandEnv",
    "Function",
    "Block",
    "Data",
    "Annotation",
    "Label",
    "PublicSymbol",
    "UDT",
    "Enum",
    "FunctionType",
    "PointerType",
    "ArrayType",
    "BaseType",
    "Typedef",
    "BaseClass",
    "Friend",
    "FunctionArgType",
    "FuncDebugStart",
    "FuncDebugEnd",
    "UsingNamespace",
    "VTableShape",
    "VTable",
    "Custom",
    "Thunk",
    "CustomType",
    "ManagedType",
    "Dimension",
    "CallSite",
    "InlineSite",
    "BaseInterface",
    "VectorType",
    "MatrixType",
    "HLSLType",
    "Caller",
    "Callee",
    "Export",
    "HeapAllocationSite",
    "CoffGroup",
    "Max"
};


// BaseType, originally from CVCONST.H in DIA SDK 
enum BaseTypeEnumType
{
    btNoType = 0,
    btVoid = 1,
    btChar = 2,
    btCHAR = 3,
    btInt = 6,
    btUInt = 7,
    btFloat = 8,
    btBCD = 9,
    btBool = 10,
    btLong = 13,
    btULong = 14,
    btCurrency = 25,
    btDate = 26,
    btVariant = 27,
    btComplex = 28,
    btBit = 29,
    btBSTR = 30,
    btHresult = 31
};

const char* BaseTypeEnumName[] = 
{
    "none",
    "void",
    "char",
    "wchar",
    "int",
    "uint",
    "floating-point-number",
    "binary-coded-decimal",
    "bool",
    "long",
    "ulong",
    "currency",
    "date",
    "variable-type-structure",
    "complex-number",
    "bit",
    "basic/binary-string",
    "HRESULT"
};


// UDtKind, originally from CVCONST.H in DIA SDK 
enum EUdtKind
{
    UdtStruct,
    UdtClass,
    UdtUnion
};

// CV_call_e, originally from CVCONST.H in DIA SDK 
typedef enum ECV_call_e {
    CV_CALL_NEAR_C      = 0x00, // near right to left push, caller pops stack
    CV_CALL_FAR_C       = 0x01, // far right to left push, caller pops stack
    CV_CALL_NEAR_PASCAL = 0x02, // near left to right push, callee pops stack
    CV_CALL_FAR_PASCAL  = 0x03, // far left to right push, callee pops stack
    CV_CALL_NEAR_FAST   = 0x04, // near left to right push with regs, callee pops stack
    CV_CALL_FAR_FAST    = 0x05, // far left to right push with regs, callee pops stack
    CV_CALL_SKIPPED     = 0x06, // skipped (unused) call index
    CV_CALL_NEAR_STD    = 0x07, // near standard call
    CV_CALL_FAR_STD     = 0x08, // far standard call
    CV_CALL_NEAR_SYS    = 0x09, // near sys call
    CV_CALL_FAR_SYS     = 0x0a, // far sys call
    CV_CALL_THISCALL    = 0x0b, // this call (this passed in register)
    CV_CALL_MIPSCALL    = 0x0c, // Mips call
    CV_CALL_GENERIC     = 0x0d, // Generic call sequence
    CV_CALL_ALPHACALL   = 0x0e, // Alpha call
    CV_CALL_PPCCALL     = 0x0f, // PPC call
    CV_CALL_SHCALL      = 0x10, // Hitachi SuperH call
    CV_CALL_ARMCALL     = 0x11, // ARM call
    CV_CALL_AM33CALL    = 0x12, // AM33 call
    CV_CALL_TRICALL     = 0x13, // TriCore Call
    CV_CALL_SH5CALL     = 0x14, // Hitachi SuperH-5 call
    CV_CALL_M32RCALL    = 0x15, // M32R Call
    CV_CALL_RESERVED    = 0x16  // first unused call enumeration
} CV_call_e;

// DataKind, originally from CVCONST.H in DIA SDK 
enum DataKindEnumType
{
    DataIsUnknown,
    DataIsLocal,
    DataIsStaticLocal,
    DataIsParam,
    DataIsObjectPtr,
    DataIsFileStatic,
    DataIsGlobal,
    DataIsMember,
    DataIsStaticMember,
    DataIsConstant
};
const char* DataKindEnumName[] = 
{
    "Unknown",
    "Local",
    "StaticLocal",
    "Param",
    "ObjectPtr",
    "FileStatic",
    "Global",
    "Member",
    "StaticMember",
    "Constant"
};


// CV_HREG_e, originally from CVCONST.H in DIA SDK 
typedef enum ECV_HREG_e {

    // Only a limited number of registers included here 

    CV_REG_EAX      =  17,
    CV_REG_ECX      =  18,
    CV_REG_EDX      =  19,
    CV_REG_EBX      =  20,
    CV_REG_ESP      =  21,
    CV_REG_EBP      =  22,
    CV_REG_ESI      =  23,
    CV_REG_EDI      =  24,

} CV_HREG_e;

// LocatonType, originally from CVCONST.H in DIA SDK 
enum ELocationType
{
    LocIsNull,
    LocIsStatic,
    LocIsTLS,
    LocIsRegRel,
    LocIsThisRel,
    LocIsEnregistered,
    LocIsBitField,
    LocIsSlot,
    LocIsIlRel,
    LocInMetaData,
    LocIsConstant,
    LocTypeMax
};

#endif // _NO_CVCONST_H


// Helper functions 

///////////////////////////////////////////////////////////////////////////////
// Functions 
//

bool GetFileSize( const char* pFileName, DWORD& FileSize )
{
    // Check parameters 

    if( pFileName == 0 ) 
    {
        return false; 
    }


    // Open the file 

    HANDLE hFile = ::CreateFile( pFileName, GENERIC_READ, FILE_SHARE_READ, 
                                 NULL, OPEN_EXISTING, 0, NULL ); 

    if( hFile == INVALID_HANDLE_VALUE ) 
    {
        ecs_os_dbg("CreateFile() failed. Error: %u ", ::GetLastError() ); 
        return false; 
    }


    // Obtain the size of the file 

    FileSize = ::GetFileSize( hFile, NULL ); 

    if( FileSize == INVALID_FILE_SIZE ) 
    {
        ecs_os_dbg("GetFileSize() failed. Error: %u ", ::GetLastError() ); 
        // and continue ... 
    }


    // Close the file 

    if( !::CloseHandle( hFile ) ) 
    {
        ecs_os_dbg("CloseHandle() failed. Error: %u ", ::GetLastError() ); 
        // and continue ... 
    }


    // Complete 

    return ( FileSize != INVALID_FILE_SIZE ); 

}

bool GetFileParams( const char* pFileName, DWORD& BaseAddr, DWORD& FileSize ) 
{
    // Check parameters 

    if( pFileName == 0 ) 
    {
        return false; 
    }


    // Determine the extension of the file 

    char szFileExt[_MAX_EXT] = {0}; 

    _splitpath( pFileName, NULL, NULL, NULL, szFileExt ); 

    
    // Is it .PDB file ? 

    if( _stricmp( szFileExt, ".PDB" ) == 0 ) 
    {
        // Yes, it is a .PDB file 

        // Determine its size, and use a dummy base address 

        BaseAddr = 0x10000000; // it can be any non-zero value, but if we load symbols 
                               // from more than one file, memory regions specified 
                               // for different files should not overlap 
                               // (region is "base address + file size") 
        
        if( !GetFileSize( pFileName, FileSize ) ) 
        {
            return false; 
        }

    }
    else 
    {
        // It is not a .PDB file 

        // Base address and file size can be 0 

        BaseAddr = 0; 
        FileSize = 0; 
    }


    // Complete 

    return true; 

}

///////////////////////////////////////////////////////////////////////////////
// main 
//

struct ecs_cry_dbg_ctx
{
    ecs* self;
    
    HANDLE process_handle;
    DWORD module_base;
    
    SYMBOL_INFO_PACKAGE symbol_info_package;
};

UINT ecs_cry_dbg_count(ecs_cry_dbg_ctx* ctx, SymTagEnumType target_tag, ULONG* type_id, SymTagEnumType* type_tag);
bool ecs_cry_dbg_get_meta(ecs_cry_dbg_ctx* ctx, ULONG type_id, ecs_entity_t* component);
bool ecs_cry_dbg_set_meta(ecs_cry_dbg_ctx* ctx, ULONG type_id, ecs_entity_t& component);


TI_FINDCHILDREN_PARAMS* ecs_dbg_get_type_children(
    HANDLE handle,
    DWORD64 base,
    ULONG type_index
) {
    TI_FINDCHILDREN_PARAMS* children = 0;
    DWORD count;

    if (SymGetTypeInfo(handle, base, type_index, TI_GET_CHILDRENCOUNT, &count))
    {
        if (count > 0)
        {
            children = (TI_FINDCHILDREN_PARAMS*)malloc(sizeof(TI_FINDCHILDREN_PARAMS) + count * sizeof(ULONG));
            children->Start = 0;
            children->Count = count;

            if (!SymGetTypeInfo(handle, base, type_index, TI_FINDCHILDREN, children))
            {
                free(children);
                children = 0;
            }
        }
    }
    
    return children;
}

bool ecs_dbg_get_type_name(
    HANDLE handle,
    DWORD64 base,
    ULONG type_index,
    char* name,
    int size
) {
    WCHAR* wname = 0;

    if (SymGetTypeInfo(handle, base, type_index, TI_GET_SYMNAME, &wname))
    {
        WideCharToMultiByte(
            CP_ACP,             // ANSI code page
            WC_COMPOSITECHECK,  // Check for accented characters
            wname,              // Source Unicode string
            -1,                 // -1 means string is zero-terminated
            name,               // Destination char string
            size,               // Size of buffer
            NULL,               // No default character
            NULL                // Don't care about this flag
        );

        LocalFree(wname);

        return true;
    }
    
    return false;
}

bool ecs_dbg_get_meta_primitive(
    ecs_cry_dbg_ctx* ctx,
    ULONG type_index,    
    UINT pointers,
    EcsMetaPrimitiveKind* kind
) {
	ULONG64 byte_size = 0; 

	if(!SymGetTypeInfo( ctx->process_handle, ctx->module_base, type_index, TI_GET_LENGTH, &byte_size ))
    {
        return false;
    }
    
	ULONG64 bit_size = byte_size * 8; 

    BaseTypeEnumType base_type = btNoType; 

    if(!SymGetTypeInfo( ctx->process_handle, ctx->module_base, type_index, TI_GET_BASETYPE, &base_type ) )
    {
        return false;
    }

    switch(base_type)
    {
        case btInt: 
        case btLong:
            switch(bit_size) {
                case 8:  *kind = EcsI8;  break;
                case 16: *kind = EcsI16; break;
                case 32: *kind = EcsI32; break;
                case 64: *kind = EcsI64; break;
            } break;
        
        case btBCD:
        case btUInt:
        case btULong:
            switch(bit_size) {
                case 8:  *kind = EcsU8;  break;
                case 16: *kind = EcsU16; break;
                case 32: *kind = EcsU32; break;
                case 64: *kind = EcsU64; break;
            } break;
        
        case btFloat:
            switch(bit_size) {
                case 32: *kind = EcsF32; break;
                case 64: *kind = EcsF64; break;
            } break;
        
        case btBool: *kind = EcsBool; break;

        case btBit:
        case btBSTR:
        case btChar: *kind = pointers > 0 ? EcsString : EcsChar; break;

        case btCurrency:
        case btDate:
        case btVariant:
        case btComplex:
        case btCHAR:
        case btHresult:
        case btVoid: *kind = EcsWord; break;

        default: return false;
    }

    return true;
}

int ecs_dbg_run_module_import(
    ecs*            self,
    HANDLE          process_handle,
    const char*     module_name,
    ecs_entity_t    system
) {
    ecs_cry_dbg_ctx ctx;
    
    ctx.self = self;
    ctx.process_handle = process_handle;
    ctx.symbol_info_package.si.SizeOfStruct = sizeof(ctx.symbol_info_package.si); 
    ctx.symbol_info_package.si.MaxNameLen = sizeof(ctx.symbol_info_package.name) / sizeof(ctx.symbol_info_package.name[0]);

    char pdb[MAX_PATH]; 

    GetModuleFileName(GetModuleHandleA(module_name), pdb, MAX_PATH);

    strncpy ( &pdb[strlen(pdb) - 3], "pdb", 3 );

    BOOL bRet = FALSE; 

    // Set options 

    DWORD Options = SymGetOptions(); 

        // SYMOPT_DEBUG option asks DbgHelp to print additional troubleshooting 
        // messages to debug output - use the debugger's Debug Output window 
        // to view the messages 

    Options |= SYMOPT_DEBUG; 

    ::SymSetOptions( Options ); 


    // Initialize DbgHelp and load symbols for all modules of the current process 

    bRet = ::SymInitialize ( 
        ctx.process_handle,  // Process handle of the current process 
        NULL,                 // No user-defined search path -> use default 
        FALSE                 // Do not load symbols for modules in the current process 
    ); 

    if( !bRet ) 
    {
        ecs_os_dbg("Error: SymInitialize() failed. Error code: %u ", ::GetLastError());
        return 0; 
    }


    do 
    {
        // Determine the base address and the file size 

        DWORD   BaseAddr  = 0; 
        DWORD     FileSize  = 0; 

        if( !GetFileParams( pdb, BaseAddr, FileSize ) ) 
        {
            ecs_os_dbg("Error: Cannot obtain file parameters (internal error)." ); 
            break; 
        }


        // Load symbols for the module 

        ecs_os_dbg("Loading symbols for: %s ... ", module_name ); 

        ctx.module_base = ::SymLoadModule ( 
            ctx.process_handle,  // Process handle of the current process 
            NULL,                // Handle to the module's image file (not needed)
            (PSTR)pdb,           // Path/name of the file 
            NULL,                // User-defined short name of the module (it can be NULL) 
            BaseAddr,            // Base address of the module (cannot be NULL if .PDB file is used, otherwise it can be NULL) 
            FileSize             // Size of the file (cannot be NULL if .PDB file is used, otherwise it can be NULL) 
        ); 

        if( ctx.module_base == 0 ) 
        {
            ecs_os_dbg("Error: SymLoadModule() failed. Error code: %u ", ::GetLastError());
            break; 
        }

        ecs_os_dbg("Load address: %x ",  ctx.module_base ); 
        
        
        ecs_run(self->get_world(), system, 0, &ctx);
        

        // Unload symbols for the module 

        bRet = ::SymUnloadModule(ctx.process_handle, ctx.module_base); 

        if( !bRet )
        {
            ecs_os_dbg("Error: SymUnloadModule() failed. Error code: %u ", ::GetLastError() ); 
        }

    }
    while( 0 ); 


    // Deinitialize DbgHelp 

    bRet = ::SymCleanup( ctx.process_handle ); 

    if( !bRet ) 
    {
        ecs_os_dbg("Error: SymCleanup() failed. Error code: %u ", ::GetLastError());
        return 0; 
    }


    // Complete 

    return 0; 
}

UINT ecs_cry_dbg_count(ecs_cry_dbg_ctx* ctx, SymTagEnumType target_tag, ULONG* type_id, SymTagEnumType* type_tag)
{
    UINT count = 0;

    if (*type_tag == target_tag)
    {
        ++count;

        while(SymGetTypeInfo(ctx->process_handle, ctx->module_base, *type_id, TI_GET_TYPEID, type_id))
        {
            if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, *type_id, TI_GET_SYMTAG, type_tag))
            {
                if (*type_tag == target_tag)
                {
                    ++count;
                }
            }

            break;
        }
    }

    return count;
}

bool ecs_cry_dbg_get_meta(ecs_cry_dbg_ctx* ctx, ULONG type_id, ecs_entity_t* component)
{
    SymTagEnumType type_tag;

    if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, type_id, TI_GET_SYMTAG, &type_tag))
    {
        CHAR name[64];
        UINT ptrs = ecs_cry_dbg_count(ctx, SymTagPointerType, &type_id, &type_tag);

        ULONG64 size = 0;

        ecs_entity_t entity = 0;

        memset(name, 0, sizeof(name));

        EcsMetaPrimitiveKind primitive;

        if (ecs_dbg_get_meta_primitive(ctx, type_id, ptrs, &primitive))
        {
            entity = ecs_lookup(ctx->self->get_world(), EcsMetaPrimitiveIds[primitive]);
        }
        else if (ecs_dbg_get_type_name(ctx->process_handle, ctx->module_base, type_id, name, sizeof(name)))
        { 
            entity = ecs_lookup(ctx->self->get_world(), name);

            if (entity == EcsInvalid && ptrs > 0)
            {
                entity = ecs_lookup(ctx->self->get_world(), EcsMetaPrimitiveIds[EcsWord]);
            }
        }

        if (entity)
        {
            *component = entity;

            return true;
        }

        if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, type_id, TI_GET_LENGTH, &size))
        {
            if (name[0])
            {
                entity = ecs_new_component(ctx->self->get_world(), ecs_os_strdup(name), size);
            }

            if (ecs_cry_dbg_set_meta(ctx, type_id, entity))
            {
                *component = entity;
            }
            
            return true;
        }
    }

    return false;
}

bool ecs_cry_dbg_set_meta(ecs_cry_dbg_ctx* ctx, ULONG type_id, ecs_entity_t& component)
{
    SymTagEnumType type_tag;

    if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, type_id, TI_GET_SYMTAG, &type_tag))
    {
        UINT ptrs = ecs_cry_dbg_count(ctx, SymTagPointerType, &type_id, &type_tag);

        switch(type_tag)
        {
            case SymTagTypedef: { 
                if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, type_id, TI_GET_TYPEID, &type_id))
                {
                    return ecs_cry_dbg_set_meta(ctx, type_id, component);
                }
            } break;

            case SymTagUDT: {
                EcsMetaStruct meta_struct;
                
                ecs_vector_params_t meta_member_params;
                
                memset(&meta_struct, 0, sizeof(meta_struct));
                memset(&meta_member_params, 0, sizeof(meta_member_params));

                meta_member_params.element_size = sizeof(EcsMetaMember);

                TI_FINDCHILDREN_PARAMS* members = ecs_dbg_get_type_children(ctx->process_handle, ctx->module_base, type_id);
                
                if (members)
                {
                    for (int j = 0; j < members->Count; ++j)
                    {
                        DWORD member_id = members->ChildId[j];
                        DWORD member_type_id;
                        
                        if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, member_id, TI_GET_TYPEID, &member_type_id))
                        {
                            EcsMetaMember meta_member;

                            memset(&meta_member, 0, sizeof(meta_member));

                            meta_member.name = new char[64];

                            if (ecs_dbg_get_type_name(ctx->process_handle, ctx->module_base, member_id, const_cast<char*>(meta_member.name), 64))
                            {
                                if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, member_id, TI_GET_OFFSET, &meta_member.offset))
                                {
                                    if (ecs_cry_dbg_get_meta(ctx, member_type_id, &meta_member.type))
                                    {
                                        *((EcsMetaMember*)ecs_vector_add(&meta_struct.members, &meta_member_params)) = meta_member;

                                        continue;
                                    }
                                }
                            }

                            delete[] meta_member.name;
                        }
                    }

                    free(members);
                }
                
                ctx->self->set(component, meta_struct);
                //FIXME
                //ctx->self->add<EcsMetaDefined>(component);

                return true;
            } break;

            case SymTagEnum: {

            } break;

            case SymTagFunctionType: {

            } break;

            case SymTagArrayType: {
                EcsMetaArray meta_array;

                if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, type_id, TI_GET_COUNT, &meta_array.size))
                {
                    if (SymGetTypeInfo(ctx->process_handle, ctx->module_base, type_id, TI_GET_TYPEID, &type_id))
                    {
                        if (ecs_cry_dbg_get_meta(ctx, type_id, &meta_array.element_type))
                        {
                            EcsId       element_id = ecs_get_id(ctx->self->get_world(), meta_array.element_type);
                            uint16_t    element_size = 0;

                            ecs_strbuf_t array_id;
                            
                            memset(&array_id, 0, sizeof(array_id));
                            
                            ecs_strbuf_append(&array_id, "%s[%d]", element_id, meta_array.size);
                            
                            EcsId id = ecs_strbuf_get(&array_id);
                            
                            component = ecs_lookup(ctx->self->get_world(), id);

                            if (component == EcsInvalid)
                            {
                                component = ecs_new_component(ctx->self->get_world(), id, element_size * meta_array.size);

                                ctx->self->set(component, meta_array);
                                //FIXME
                                //ctx->self->add<EcsMetaDefined>(component);
                            }

                            return true;
                        }
                    }
                }
            } break;

            case SymTagBaseType: {
                EcsMetaPrimitive meta_primitive;
                
                if (ecs_dbg_get_meta_primitive(ctx, type_id, ptrs, &meta_primitive.kind))
                {
                    ctx->self->set(component, meta_primitive);
                    //FIXME
                    //ctx->self->add<EcsMetaDefined>(component);

                    return true;
                }
            } break;
        }
    }

    return false;
}

template<typename T>
const T& cast_next(void*& ptr) {
    const T& val = *(const T*)(ptr);
        
    ptr = (void*)((intptr_t)(ptr) + sizeof(T));

    return val;
}

void ecs_cry_on_type_info(ecs_rows_t* rows)
{
    ecs_cry_dbg_ctx* ctx = (ecs_cry_dbg_ctx*)rows->param;

    ECS_COLUMN(rows, EcsId, id, 1);

    for (uint32_t i = 0; i < rows->count; ++i)
    { 
        LPSTR symbol_name = (LPSTR)id[i];

        if (SymGetTypeFromName(ctx->process_handle, ctx->module_base, symbol_name, &ctx->symbol_info_package.si))
        {
            ecs_cry_dbg_set_meta(ctx, ctx->symbol_info_package.si.TypeIndex, rows->entities[i]);
        }
    }
}

void ecs_cry_dbg_system_status(
    ecs_world_t *world,
    ecs_entity_t system,
    ecs_system_status_t status,
    void *ctx
) {
    const char* names[] = {
        "none",
        "enabled",
        "disabled",
        "activated",
        "deactivated"
    };

    EcsId id = ecs_get_id(world, system);

    ecs_os_dbg(" > %s: %s (system)", names[status], id);
}

void ecs_cry_dbg_entity_to_json(
    ecs_world_t *world,
    ecs_strbuf_t* buf,
    ecs_entity_t entity
) {
    ecs_dbg_entity_t dbg;
    ecs_dbg_entity(world, entity, &dbg);

    ecs_dbg_table_t dbg_table = {0};
    if (dbg.table) {
        ecs_dbg_table(world, dbg.table, &dbg_table);
    }    

    ecs_strbuf_list_push(buf, "{", ",");

    ecs_strbuf_list_append(buf, "\"id\": \"%s\"", ecs_get_id(world, entity));

    if (dbg_table.systems_matched) {
        ecs_entity_t *systems = (ecs_entity_t *)ecs_vector_first(dbg_table.systems_matched);
        uint32_t i, count = ecs_vector_count(dbg_table.systems_matched);

        ecs_strbuf_list_append(buf, "\"systems\":");

        ecs_strbuf_list_push(buf, "[", ",");

        for (i = 0; i < count; i ++) {
            ecs_entity_t s = systems[i];
            EcsId sid = ecs_get_id(world, s);
            
            ecs_strbuf_list_append(buf, "\"%s\"", sid);
        }
        
        ecs_strbuf_list_pop(buf, "]");
    }

    if (dbg_table.type) {
        ecs_entity_t *components = (ecs_entity_t *)ecs_vector_first(dbg_table.type);
        uint32_t i, count = ecs_vector_count(dbg_table.type);

        ecs_strbuf_list_append(buf, "\"archetype\":");

        ecs_strbuf_list_push(buf, "{", ",");

        for (i = 0; i < count; i ++) {
            ecs_entity_t    c = components[i];
            ecs_type_t      ct = ecs_type_from_entity(world, c);
            EcsId           cid = ecs_get_id(world, c);
            void*           cptr = _ecs_get_ptr(world, entity, ct);

            ecs_strbuf_list_append(buf, "\"%s\":", cid);

            ecs_strbuf_list_push(buf, "{", ",");

            if (cptr) {
                ecs_strbuf_list_append(buf, "\"address\": \"0x%p\"", cptr);
                ecs_strbuf_list_append(buf, "\"component\":");

                ecs_cry_dbg_component_to_json(world, buf, c, cptr);
            }
            
            ecs_strbuf_list_pop(buf, "}");
        }
        ecs_strbuf_list_pop(buf, "}");
    }
    
    ecs_strbuf_list_pop(buf, "}");
}

bool ecs_cry_dbg_component_to_json(
    ecs_world_t *world, 
    ecs_strbuf_t *buf, 
    ecs_entity_t component,
    void*& ptr
) {
    EcsMetaType* meta_type = ecs::get(world)->get<EcsMetaType>(component);

    if (!meta_type) {
        return false;
    }
    
    switch(meta_type->kind) {
        case EcsPrimitive: {
            EcsMetaPrimitive *meta = ecs::get(world)->get<EcsMetaPrimitive>(component);

            if (meta) {

                switch (meta->kind) {
                    case EcsBool: {
                        ecs_strbuf_append(buf, "%d", cast_next<bool>(ptr));
                    } break;

                    case EcsChar: {
                        ecs_strbuf_append(buf, "%c", cast_next<char>(ptr));
                    } break;
                    
                    case EcsByte: {
                        ecs_strbuf_append(buf, "%d", cast_next<char>(ptr));
                    } break;
                    
                    case EcsU8: {
                        ecs_strbuf_append(buf, "%I8u", cast_next<uint8_t>(ptr));
                    } break;
                    
                    case EcsU16: {
                        ecs_strbuf_append(buf, "%I16u", cast_next<uint16_t>(ptr));
                    } break;
                    
                    case EcsU32: {
                        ecs_strbuf_append(buf, "%I32u", cast_next<uint32_t>(ptr));
                    } break;
                    
                    case EcsU64: {
                        ecs_strbuf_append(buf, "%I64u", cast_next<uint64_t>(ptr));
                    } break;
                    
                    case EcsI8: {
                        ecs_strbuf_append(buf, "%I8", cast_next<int8_t>(ptr));
                    } break;
                    
                    case EcsI16: {
                        ecs_strbuf_append(buf, "%I16", cast_next<int16_t>(ptr));
                    } break;
                    
                    case EcsI32: {
                        ecs_strbuf_append(buf, "%I32", cast_next<int32_t>(ptr));
                    } break;
                    
                    case EcsI64: {
                        ecs_strbuf_append(buf, "%I64", cast_next<int64_t>(ptr));
                    } break;
                    
                    case EcsF32: {
                        ecs_strbuf_append(buf, "%f", cast_next<float>(ptr));
                    } break;
                    
                    case EcsF64: {
                        ecs_strbuf_append(buf, "%f", cast_next<double>(ptr));
                    } break;
                    
                    case EcsWord: {
                        ecs_strbuf_append(buf, "%d", cast_next<intptr_t>(ptr));
                    } break;
                    
                    case EcsString: {
                        ecs_strbuf_append(buf, "\"%s\"", cast_next<const char*>(ptr));
                    } break;
                    
                    case EcsEntity: {
                        ecs_strbuf_append(buf, "\"%s (entity)\"", ecs_get_id(world, cast_next<ecs_entity_t>(ptr)));
                    } break;
                                                    
                default:
                    break;
                }
            }
        } break;

        case EcsArray: {
            EcsMetaArray *meta = ecs::get(world)->get<EcsMetaArray>(component);

            if (meta) {
                ecs_strbuf_list_push(buf, "[", ",");

                for (uint16_t i = 0; i < meta->size; ++i) {
                    ecs_strbuf_list_next(buf);

                    ecs_cry_dbg_component_to_json(world, buf, meta->element_type, ptr);
                }
                ecs_strbuf_list_pop(buf, "]");
            }
        } break;

        case EcsStruct: {
            EcsMetaStruct *meta = ecs::get(world)->get<EcsMetaStruct>(component);
            
            if (meta && meta->members) {
                size_t count = ecs_vector_count(meta->members);
                EcsMetaMember* members = (EcsMetaMember*)ecs_vector_first(meta->members);

                ecs_strbuf_list_push(buf, "{", ",");

                for (size_t i = 0; i < count; ++i) {
                    EcsMetaMember& member = members[i];
                    
                    void* mptr = (void*)((intptr_t)ptr + member.offset);

                    ecs_strbuf_list_append(buf, "\"%s\":", member.name);
                    
                    ecs_cry_dbg_component_to_json(world, buf, member.type, mptr);
                }
                
                ecs_strbuf_list_pop(buf, "}");
            }
        } break;

        case EcsBitmask:
        case EcsEnum:
        case EcsVector:
        case EcsMap:                    
        default: {
            ecs_strbuf_append(buf, "\"(%s)\"", EcsMetaTypeName[meta_type->kind]);
        } break;
    }

    return true;
}


void ecs_cry_dbg_integrate(ecs* self, int flags)
{
    ecs_entity_t type_info = self->new_system_builder()
        .self<ecs_component::EcsId>()
        .self<ecs_component::EcsComponent>()
        .and().not().typeof<EcsMetaDefined>()
        .build("type_info", EcsManual, &ecs_cry_on_type_info)
    ;

    ecs_dbg_run_module_import(self, GetCurrentProcess(), "CryGame.dll", type_info);
}