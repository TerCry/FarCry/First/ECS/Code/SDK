#pragma once

#include "ecs.hpp"
#include "ecs-modules.hpp"


struct ecs_cry_window
{
    EcsMat3x3 screen; /* viewport coords to screen coords */
    EcsMat3x3 projection; /* screen + camera transform */
    EcsVec2 scale; /* when only scaling is required */
    EcsVec2 coeff;
};

void ecs_cry_window_integrate(ecs* self, int flags);