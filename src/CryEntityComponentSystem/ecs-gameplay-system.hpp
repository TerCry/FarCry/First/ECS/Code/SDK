#pragma once

#include "ecs.hpp"


ecs_define_tag(,app_state_play);
ecs_define_tag(,gameplay_entity);
ecs_define_tag(,gameplay_system);

struct ecs_cry_gameplay
{
    ecs_entity_t localplayer;
};

void ecs_cry_gameplay_integrate(ecs* self, int flags);
void ecs_cry_game_launch(ecs* self);