#pragma once

struct ecs_rows_t;

void ecs_cry_do_disable_systems(ecs_rows_t *rows);
void ecs_cry_do_enable_systems(ecs_rows_t *rows);
void ecs_cry_do_disable_entities(ecs_rows_t *rows);
void ecs_cry_do_enable_entities(ecs_rows_t *rows);