#include "ecs-render-system.hpp"
#include "ecs-window-system.hpp"

#include <ISystem.h>
#include <IRenderer.h>
#include <IConsole.h>

void ecs_cry_render_line(
    ecs_cry_window *wnd, 
    const EcsVec2& from, 
    const EcsVec2& to, 
    EcsLineColor *color
) {
    IRenderer *renderer = GetISystem()->GetIRenderer();

    const float kfColorRatio(1.0f / 255.0f);

    renderer->SetMaterialColor(
        kfColorRatio * color->r,
        kfColorRatio * color->g,
        kfColorRatio * color->b,
        kfColorRatio * color->a
    );
    renderer->SetLineWidth(10.0f);
    renderer->Draw2dLine(
        wnd->coeff.x * from.x,	wnd->coeff.y * from.y, 
        wnd->coeff.x * to.x,	wnd->coeff.y * to.y
    );
}
void ecs_cry_render_image(
    ecs_cry_window *wnd,
    const EcsVec2& pos, 
    EcsColor* color,
    float w, 
    float h, 
    int texture_id,
    float s0=0, float t0=0,
    float s1=1, float t1=1,
    float angle=0,
    float z=1
) {
    IRenderer *renderer = GetISystem()->GetIRenderer();

    const float kfColorRatio(1.0f / 255.0f);
    const float kfHalfRatio(1.0f / 2.0f);

    GetISystem()->GetIRenderer()->Draw2dImage(
        pos.x - kfHalfRatio * w, 
        pos.y - kfHalfRatio * h, 
        w, h,
        texture_id, 
        s0,t0,
        s1,t1,
        angle,
        kfColorRatio * color->r,
        kfColorRatio * color->g,
        kfColorRatio * color->b,
        kfColorRatio * color->a,
        z
    );
}

void ecs_cry_render_rectangle(
    ecs_cry_window *wnd,
    EcsMatTransform2D *m,
    uint32_t width,
    uint32_t height,
    EcsColor *c,
    EcsLineColor *lc)
{
    if (!c && !lc) {
        c = &ecs_color_WHITE;
    }

    if (c) {
        int w = width * wnd->scale.x;
        int h = height * wnd->scale.y;

        EcsVec2 position[1] = {
            {0, 0}
        };

        ecs_mat3x3_transform(m, position, position, 1);
        ecs_mat3x3_transform(&wnd->projection, position, position, 1);

        ecs_cry_render_image(wnd, position[0], c, w, h, -1);
    }

    if (lc) {
        int w = width / 2.0;
        int h = height / 2.0;

        EcsVec2 points[5];
        points[0].x = -w;
        points[0].y = -h;
        points[1].x = w;
        points[1].y = -h;
        points[2].x = w;
        points[2].y = h;
        points[3].x = -w;
        points[3].y = h;
        points[4].x = -w;
        points[4].y = -h;

        ecs_mat3x3_transform(m, points, points, 5);
        ecs_mat3x3_transform(&wnd->projection, points, points, 5);

        ecs_cry_render_line(wnd, points[0], points[1], lc);
        ecs_cry_render_line(wnd, points[1], points[2], lc);
        ecs_cry_render_line(wnd, points[2], points[3], lc);
        ecs_cry_render_line(wnd, points[3], points[4], lc);
    }
}

void ecs_cry_do_render_line(ecs_rows_t *rows)
{
    ecs_cry_window *wnd = (ecs_cry_window *)rows->param;
    int i;
    for (i = 0; i < rows->count; i ++) {
        EcsLine *shape = ecs_field(rows, EcsLine, 1, i);
        EcsMatTransform2D *m = ecs_field(rows, EcsMatTransform2D, 2, i);
        EcsLineColor *c = ecs_field(rows, EcsLineColor, 3, i);
        

        EcsVec2 position[2] = {
            {shape->start.x, shape->start.y},
            {shape->stop.x, shape->stop.y}
        };

        ecs_mat3x3_transform(m, position, position, 2);
        ecs_mat3x3_transform(&wnd->projection, position, position, 2);

        ecs_cry_render_line(wnd, position[0], position[1], c);
    }
}
void ecs_cry_do_render_rectangle(ecs_rows_t *rows)
{
    ecs_cry_window *wnd = (ecs_cry_window *)rows->param;

    int i;
    for (i = 0; i < rows->count; i ++) {
        EcsRectangle *shape = ecs_field(rows, EcsRectangle, 1, i);
        EcsMatTransform2D *m = ecs_field(rows, EcsMatTransform2D, 2, i);
        EcsColor *c = ecs_field(rows, EcsColor, 3, i);
        EcsLineColor *lc = ecs_field(rows, EcsLineColor, 4, i);
        
        ecs_cry_render_rectangle(wnd, m, shape->width, shape->height, c, lc);  
    }
}
void ecs_cry_do_render_square(ecs_rows_t *rows)
{
    ecs_cry_window *wnd = (ecs_cry_window *)rows->param;

    int i;
    for (i = 0; i < rows->count; i ++) {
        EcsSquare *shape = ecs_field(rows, EcsSquare, 1, i);
        EcsMatTransform2D *m = ecs_field(rows, EcsMatTransform2D, 2, i);
        EcsColor *c = ecs_field(rows, EcsColor, 3, i);
        EcsLineColor *lc = ecs_field(rows, EcsLineColor, 4, i);
      
        ecs_cry_render_rectangle(wnd, m, shape->size, shape->size, c, lc);  
    }
}
void ecs_cry_do_render_circle(ecs_rows_t *rows)
{
    IRenderer *renderer = GetISystem()->GetIRenderer();

    int i;
    for (i = 0; i < rows->count; i ++) {
        EcsCircle *shape = ecs_field(rows, EcsCircle, 1, i);
        EcsMatTransform2D *m = ecs_field(rows, EcsMatTransform2D, 2, i);
        EcsColor *c = ecs_field(rows, EcsColor, 3, i);
        EcsLineColor *lc = ecs_field(rows, EcsLineColor, 4, i);
        //TODO: implement
    }
}
void ecs_cry_do_render_polygon8(ecs_rows_t *rows)
{
    IRenderer *renderer = GetISystem()->GetIRenderer();

    int i;
    for (i = 0; i < rows->count; i ++) {
        EcsPolygon8 *shape = ecs_field(rows, EcsPolygon8, 1, i);
        EcsMatTransform2D *m = ecs_field(rows, EcsMatTransform2D, 2, i);
        EcsColor *c = ecs_field(rows, EcsColor, 3, i);
        EcsLineColor *lc = ecs_field(rows, EcsLineColor, 4, i);
        //TODO: implement
    }
}
void ecs_cry_do_render_dot(ecs_rows_t *rows)
{
    IRenderer *renderer = GetISystem()->GetIRenderer();

    int i;
    for (i = 0; i < rows->count; i ++) {
        EcsMatTransform2D *m = ecs_field(rows, EcsMatTransform2D, 2, i);
        EcsColor *c = ecs_field(rows, EcsColor, 3, i);
        //TODO: implement
    }
}

void ecs_cry_do_render(ecs_rows_t* rows)
{
    IRenderer *renderer = GetISystem()->GetIRenderer();

    ecs_world_t *world = rows->world;
    ecs_cry_window *wnd = ecs_column(rows, ecs_cry_window, 1);
    EcsCanvas2D *canvas = ecs_column(rows, EcsCanvas2D, 2);

    int 
        w = canvas->window_actual.width, 
        h = canvas->window_actual.height
    ;

    
    wnd->coeff.x = float(w) / renderer->GetWidth();
    wnd->coeff.y = float(h) / renderer->GetHeight();


    renderer->SetClearColor(Vec3(zero));
    GetISystem()->GetIConsole()->Clear();
    GetISystem()->GetIConsole()->ShowConsole(false);
    int iViewportX, iViewportY, iViewportW, iViewportH;
    int i, j;
    for (i = 0; i < rows->count; i ++) {
        GetISystem()->RenderBegin();
        renderer->GetViewport(&iViewportX, &iViewportY, &iViewportW, &iViewportH);	
        renderer->Set2DMode(1, w, h);
        renderer->SetViewport(0, 0, w, h);
        renderer->SetScissor(0, 0, w, h);
        renderer->ClearDepthBuffer();
        renderer->ClearColorBuffer(Vec3(zero));
        renderer->SetState(GS_BLSRC_SRCALPHA | GS_BLDST_ONEMINUSSRCALPHA | GS_NODEPTHTEST);
        renderer->SetCullMode(R_CULL_DISABLE);
        renderer->SetMaterialColor(1, 1, 1, 1);

        for (j = 2; j < rows->column_count - 1; ++j) {
            ecs_run(world, rows->components[j], 0, wnd);
        }
        
        renderer->SetViewport(iViewportX, iViewportY, iViewportW, iViewportH);	
        renderer->Set2DMode(0, 0, 0);
        GetISystem()->RenderEnd();
    }
}

void ecs_cry_renderer_integrate(ecs* self, int flags)
{
    ecs_entity_t render_line = self->new_system_builder()
        .self<ecs_component::EcsLine>()
        .self<ecs_component::EcsMatTransform2D>()
        .and().optional().typeof<ecs_component::EcsLineColor>()
        .system<ecs_component::EcsHidden>()
        .build("render_line", EcsManual, &ecs_cry_do_render_line)
    ;
    
    ecs_entity_t render_square = self->new_system_builder()
        .self<ecs_component::EcsSquare>()
        .self<ecs_component::EcsMatTransform2D>()
        .and().optional().typeof<ecs_component::EcsColor>()
        .and().optional().typeof<ecs_component::EcsLineColor>()
        .system<ecs_component::EcsHidden>()
        .build("render_square", EcsManual, &ecs_cry_do_render_square)
    ;

    ecs_entity_t render_rectangle = self->new_system_builder()
        .self<ecs_component::EcsRectangle>()
        .self<ecs_component::EcsMatTransform2D>()
        .and().optional().typeof<ecs_component::EcsColor>()
        .and().optional().typeof<ecs_component::EcsLineColor>()
        .system<ecs_component::EcsHidden>()
        .build("render_rectangle", EcsManual, &ecs_cry_do_render_rectangle)
    ;
    
    ecs_entity_t render_circle = self->new_system_builder()
        .self<ecs_component::EcsCircle>()
        .self<ecs_component::EcsMatTransform2D>()
        .and().optional().typeof<ecs_component::EcsColor>()
        .and().optional().typeof<ecs_component::EcsLineColor>()
        .system<ecs_component::EcsHidden>()
        .build("render_circle", EcsManual, &ecs_cry_do_render_circle)
    ;
    
    ecs_entity_t render_polygon8 = self->new_system_builder()
        .self<ecs_component::EcsPolygon8>()
        .self<ecs_component::EcsMatTransform2D>()
        .and().optional().typeof<ecs_component::EcsColor>()
        .and().optional().typeof<ecs_component::EcsLineColor>()
        .system<ecs_component::EcsHidden>()
        .build("render_polygon8", EcsManual, &ecs_cry_do_render_polygon8)
    ;
    
    ecs_entity_t render_dot = self->new_system_builder()
        .self<ecs_component::EcsDot>()
        .self<ecs_component::EcsMatTransform2D>()
        .and().optional().typeof<ecs_component::EcsColor>()
        .system<ecs_component::EcsHidden>()
        .build("render_dot", EcsManual, &ecs_cry_do_render_dot)
    ;
    
    self->new_system_builder()
        .self<ecs_cry_window>()
        .self<ecs_component::EcsCanvas2D>()
        .nothing(render_square)
        .nothing(render_rectangle)
        .nothing(render_circle)
        .nothing(render_polygon8)
        .nothing(render_dot)
        .nothing(render_line)
        .system<ecs_component::EcsHidden>()
        .build("render", EcsOnStore, &ecs_cry_do_render)
    ;
}
