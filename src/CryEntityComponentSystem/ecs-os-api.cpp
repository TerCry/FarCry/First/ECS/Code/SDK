#include "ecs.hpp"
#include "ecs-os-api.hpp"

#include <windows.h>

//////////////////////////////////////////////////////////////////////////////////////////////
// ECS os api intergration
//////////////////////////////////////////////////////////////////////////////////////////////
void ecs_cry_log(const char * __format_str, va_list __args)
{
    char __buffer [4096];
    _vsnprintf(__buffer, sizeof(__buffer) / sizeof(char),
               __format_str, __args);
    OutputDebugStringA("[log] ");
  OutputDebugStringA(__buffer);
    OutputDebugStringA("\n");
}
void ecs_cry_log_error(const char * __format_str,  va_list __args)
{
    char __buffer [4096];
    _vsnprintf(__buffer, sizeof(__buffer) / sizeof(char),
               __format_str, __args);
    OutputDebugStringA("[err] ");
    OutputDebugStringA(__buffer);
    OutputDebugStringA("\n");
}
void ecs_cry_log_debug(const char * __format_str,  va_list __args)
{
    char __buffer [4096];
    _vsnprintf(__buffer, sizeof(__buffer) / sizeof(char),
               __format_str, __args);
    OutputDebugStringA("[dbg] ");
    OutputDebugStringA(__buffer);
    OutputDebugStringA("\n");
}
void ecs_cry_abort()
{
    assert(false);
}

void ecs_cry_os_api_integrate()
{
    ecs_os_set_api_defaults();
    ecs_os_api_t api = ecs_os_api;
    api.log = &ecs_cry_log;
    api.log_error = &ecs_cry_log_error;
    api.log_debug = &ecs_cry_log_debug;
    api.abort = &ecs_cry_abort;
    ecs_os_set_api(&api);
}
